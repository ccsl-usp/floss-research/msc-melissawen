Roteiro para o vídeo de 4 min do NIER

O vídeo é usado como backup em caso de problemas de conectividade do apresentador.

## take 1

Hi everyone,

my name is Leonardo Leite, I'm a phd student at the University of São Paulo in Brazil.

I'm going to talk about promoting openly-available documents produced by Free and open source software communities in the use of software engineering research.

And this topic has been conducted mainly by misses Wen, a master student also at the University of São Paulo.

## take 2 

Open source communities produce diverse types of artifacts, including: 
technical reports, white papers, documentation, magazines, and blog posts

This kind of information, not controlled by commercial publishers, 
is also known as Grey Literature (GL).

GL has advantages of its own.
It provides more up-to-date information.
It exposes more openly negative and null results.
And helps to identify gaps in the academic literature.

Given the vast amount of public documents from open-source communities, we claim that 
researchers should investigate such resources before taking the time of contributors with surveys or interviews.

However, there is not yet a precise methodology for conducting Grey Literature Reviews (GLR), or guidelines prescribing how to cope with the specific challenges of GL. 

Based on our experience in conducting a GLR about the Linux kernel development model,

and based on Systematic Literature Review methodology,

we present, in this work, challenges and adaptations for planning, selecting data sources, and selecting documents for a GLR.

So now I'll present some of these adaptations and recommendations.

## take 3

GL includes opinion-based material.
Without care, it can even lead to fake news.
Therefore, the credibility of GL material is a major concern.

So we recommend control factors for coping with this issue.
Examples used in our review: 
1. The organization or author publishing the material has some reputation.
2. Historical facts, data, or Linux artifacts support the statements in the document. 

The variation in terminology for GL can be wider than for academic literature.
Therefore, define synonyms for your search terms.
In our review, when searching for "development model",
we expanded "model" using "culture, practices, rules, and structure".

On the other hand, much of website search engines cannot properly address complex queries. 

Therefore, try to polish your query to be both broad and simple.

Traditional reviews usually employ very well-known and prestigious digital libraries. But for GL, data sources are not so obvious. 

So you need a process for choosing data sources.
You can...
web search for data sources, 
consult experts, 
gather snowballing references from key documents, 
and select according to some classification...

For example, we divided data sources in: dealing specifically with the Linux kernel versus dealing more broadly with IT or open-source subjects.
So we prioritized the evaluation of specific data sources.

In any literature review, there is a phase for title-based selection.
However, titles in GL are often not accurate or even misleading. 
**

So we recommend a speed reading: search for the keyword to quickly understand if the document meets the selection criteria.

3'13'' até aqui

# Take 4

Following these guidelines, we observed issues that are little discussed in academic literature:
* human aspects in the maintainership structure;
* the community subsistence;
* the continuous growth of the source code
* and the non-regression rule;

We also observed divergences from traditional literature.
For example, the current development of Linux has little of anarchy and bazaar and much more of dictatorship or even feudalism.

So we hope that the strategies reported in our paper could boost the use of GLR to address open-source matters in future investigations.

Well, thank you so much for watching!

Please, feel free to ask us questions!

See you!

Total: 3'53''


