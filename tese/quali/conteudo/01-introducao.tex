%% ------------------------------------------------------------------------- %%
\chapter{Introduction}
\label{cap:introducao}

Free/Libre Open Source Software (FLOSS) development has been a subject of study
in both academia and industry. Their interest stems from the popularity of
various software projects and their successful use of ``peculiar'' management and
training practices to conduct a high-quality collaborative, geographically
distributed, community-based software development effort. However, although the search for
effective and efficient ways of producing computer programs is at the heart of
the Software Engineering (ES) discipline, the detachment from the practice observed
throughout the history of academic studies on FLOSS has led many investigations to carry
myths and preconceptions about its phenomenon and development model.

This thesis aims to mitigate the distance between what is investigated by
academia and what is observed in practice on the development model of FLOSS
projects. We\footnote{I often use ``we'' to describe the results of the collaborative process that involves my advisors and/or other researchers at the FLOSS Competence Center of the University of São Paulo (CCSL); the use of ``I'' is specific to work performed individually.}
consider that the real-world gaps in many studies on this kind of software are
not caused by lack of methodological discipline and rigor in an investigation,
but rather with the absence of research approaches that use methods able to
shed light on practice and endorse scientific findings.

Therefore, this investigation proposes to extract,
from the state-of-art and -practice, the attributes used to characterize the
development of FLOSS projects and the values that these attributes assume in
the different FLOSS ecosystems. From this mapping, we aim to confirm the
characteristics related to the development model of the Linux project in light
of the social and behavioral aspects of the workflow of its community.

\section{Problem outline}
\label{sec:problem}

In 1997, \citet{Raymond1999} described two supposedly antagonistic FLOSS development
models based on his observations of the
Linux kernel\footnote{\url{https://www.linuxfoundation.org/projects/linux}}
development and also lessons learned during the development of his own software project, \texttt{fetchmail}.
This seminal essay, named ``The Cathedral and The Bazaar'', aimed to disseminate
an apolitical and unbiased term to define the development model of free software
projects, the term \textit{open-source}. A new wave inside the free software
communities emerged and brought along with it firms interested in the benefits
of these open-source practices that enable innovation, technical support from an external community,
and ``many-eyes'' assisting the software development~\citep{Crowston2012}.

The participation of companies in Free/Libre and Open-Source Software (FLOSS)
projects has caused changes in their communities, their development processes, and also in their business strategies.
As a result, these FLOSS ecosystem elements lost several free software characteristics and became a more
conventional and commercially viable form~\citep{Fitzgerald2006}.
Many FLOSS projects are now funded by large companies or with contributions
from paid employees to develop for these projects~\citep{Lakhani2003}.
Thus, FLOSS at large became a public
product produced by the private initiative~\citep{Crowston2012}.

The Linux project also seems to have undergone transformations over this period.
Its current development model presents different aspects of those in which the
Bazaar model was based.
As a core group of developers takes most of the reviews and is also
exclusively responsible for approving code changes, passed by a chain-of-trust
to Linus Torvalds' official repository, the risk of code instability and
insertion of intractable bug on the mainline may not vary with the number of
testers and developers. This aspect resembles the design of the Cathedral
model and also contrasts with the so-called ``Linus' Law'' defined by Raymond that says
that ``Given enough eyeballs, all bugs are shallow''.

For \citet{Rigby2014}, Linux is currently managed by a dictatorship where
its review process has on average two reviewers, and 44\% of patches sent to
the project are ignored and never merged to the code.
However, for \citet{Fogel2005}, code ``forkability'', present in the Linux project, is the
reason there are not real dictators in FLOSS projects, but a special type of
dictatorship, the ``benevolent dictator'' model. In this model, the dictator
does not have a strict hold on the project, letting things work themselves out and usually making final decisions
only in the case there is no consensus.

The increase in the number of annual publications of empirical work on FLOSS
demonstrates the growing interest in the subject~\citep{Crowston2012}.
Notwithstanding the apparent consonance between industry and
academia on the absorption of FLOSS practices or products in software
development projects, \citet{Osterlie2007} reveal a lack of understanding of the
FLOSS phenomenon in academic works, exposing the homogeneous and biased point
of view of many studies and their findings. The authors consider that this imprecision
has been reproduced due to the distancing of software engineering studies
form the practice. Besides, another aggravating factor of this imprecision is
the initial and non-comprehensive use of non-formal materials, such as blog
posts, video, whitepapers, and websites, commonly produced by practitioners in
their day-to-day activities.

These mismatches led many
software engineering studies to ignore the FLOSS transformation and the
diversity inside and between projects after two decades since ``The Cathedral
and The Bazaar''.

Given this scenario, for this thesis, we delimited the scope of some terms as follows:

\begin{itemize}
\item \textit{Free Software}: refers to the Free Software movement, formalized by Richard Stallman in 1983
\item \textit{Open Source}: refers to the Open-Source movement, a movement to pitch a concept of ``free software'' development viable in the business world, as initially explicited by the Cathedral and Bazaar essay.
\item \textit{FLOSS, an acronym for Free / Libre and Open Source Software}: refers to both free and open software ecosystems. This term will be extensively used in discussions of practices and methods used by developers in the freely licensed software community.
\item \textit{FLOSS Phenomenon}: when the topic in discussion involves characteristics of the Free Software and Open-Source movements
\item \textit{FLOSS Development}: when the subject is limited to the workflow and set of techniques used in the production of this kind of software.
\end{itemize}


\section{Research Questions}
\label{sec:researchquestions}

Acknowledging the relevance of the use of FLOSS development practices for
innovation and production of quality software, the primary goal of this
research is to map the attributes that describe the contemporary FLOSS
development models considering the state-of-the-art and -practice.
To reinforce convergences and address possible divergences between what is
claimed by academia and observed in practice, we will highlight the Linux
kernel project for an in-depth case study.

The selection of the Linux project takes into account the
role played by this project in the Free Software and Open-source
movements. Also, much of the academic understanding of the FLOSS model is based
on assertions about the Linux project model -- or Bazaar model.
Describing the contemporary characteristics of the Linux project's workflow and
community, in contrast with its traditional characterization, will produce a
more realistic view of the roles, rules, and restrictions present in the Linux
development model, one of the most prominent FLOSS projects. 

To achieve the goals mentioned above, the following research questions will guide
our investigation:

\textit{\textbf{RQ1. How do software engineering studies and FLOSS development
communities describe the current models of FLOSS development and their ties to
the Linux model?}}

\textit{RQ1.1. What attributes do they use to characterize a FLOSS development
model?}

\textit{RQ1.2. Do the academia and the FLOSS development community use the same
attributes to characterize the Linux development model?}

\textit{\textbf{RQ2. How to confirm the characteristics used to describe a
particular FLOSS project development, exploring its different means of social
interaction? How to visualize socio-technical aspects that impact the
development process?}}

\section{Research Design}
\label{sec:researchdesign}

A variety of research methods supports FLOSS ecosystem studies, and the case
study is, without doubts, the most common method. Multi-method
investigations, on the other hand, are rarely used and are often designed to
incorporate interviews into case studies, surveys, and field studies~\citep{Crowston2012}.
According to \citet{Fogel2005}, it is not difficult for a project to achieve technical
success, however, after initial success, a FLOSS project needs a social
foundation and a robust developer base to handle the growth or loss of
skilled workforce.

Concerned with the poor academic experiences in the investigations of FLOSS
ecosystems and considering the growth in terms of size, diversity, and maturity of FLOSS projects over the last 21
years, this study intends to benefit from the wealth of information present in
non-academic sources and human interaction networks to provide a
socio-technical picture of contemporary FLOSS development models.

We will construct conceptual maps to describe the current understanding
of academia and industry about the FLOSS development models. After, we will
focus on the characteristics that pertain to the Linux development model. We
will conduct an ethnographic case study in the Linux project adopting quantitative and qualitative methods to collect and
analyze the data of the project under investigation: data mining of
repositories, mailing-lists, IRC channels, and issue trackers; virtual
participant observation in at least one Linux subsystem; and watching the daily
online activities of 10 others Linux subsystem developers.

Finally, we will sociotechnical analyze the characteristics of the Linux
development model from the comparison of the findings from the literature
review and the ethnographic case study.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{figure/workflow2.pdf}
	\caption{Practical and Theoretical Study Workflow}
        \label{fig:workflow}
\end{figure}

Figure\ref{fig:workflow} summarizes the research strategies, methods, expected
outcomes and the current stage of the investigation.

This research is divided into four phases: the first phase of familiarization
of data sources and research methods; the second phase of multivocal literature
review;the third phase of the ethnographic case study; and the fourth and final
phase of the confirmation of results by triangulation.
*
Each phase is detailed below.

\textbf{Phase I} is a warm-up on the studies of the FLOSS phenomenon and its
development model. At this stage, we conducted a preliminary analysis of formal
and informal literature regarding elements commonly used to describe FLOSS
development models and community workflows. These introductory materials will
support the construction of a search string for the following steps.

Another
initial step in this research is the introduction of the observer in the under
study community. To be able to collect data by participant observation, in this
phase, I also honed my code skills and started dialogues with community
members. 

From the background of the researchers in the area, we also mapped the
initial requirements for the development of a platform to collect FLOSS
community interaction logs.

\textbf{Phase II} aims to summarize the state-of-the-art and -practice via multivocal
literature review, which includes a grey literature review, a systematic review
of formal literature and, finally, the combination of findings to create conceptual
maps of FLOSS development.

First, we will develop two protocols: one to review informal narratives and
other for the formal publications. Second, we will conduct the Grey
Literature Review based on an systematic protocol and apply coding techniques to find
the attributes addressed by practitioners to characterize FLOSS development and
Linux development. In continuity, we will use the same strategy to review the
formal literature. Finally, we will combine the results of these two previous
steps into conceptual maps, identifying possible inconsistencies between theory
and practice.

\textbf{Phase III} will strengthen the findings of Phase II through in-depth
research on a specific FLOSS project. At this stage, we focus on understanding
people, cultures, and their related social and work practices to address
possible disagreements between an actual FLOSS community and academia regarding a
specific FLOSS development model.

Thus, we design a case study of the Linux kernel project where
data collection follows an ethnographic approach comprising online
participant and non-participant observation and data mining of members'
interactions of the community under study. 

For observation, on the one hand, I will participate as a developer in at least
one community of the Linux subsystems. I will write a diary of contribution,
focused on my interactions with other community members for the code
development. On another hand, I will watch the daily online activities of other
10 Linux developers, to compare their behaviors, communication styles, and
development process perceptions with mine.

For the data mining, graduate and undergraduate students of
``Advanced Topic in Object-oriented Programming'' course from the Department of Computer Science at USP, developed
an initial version of an open platform able to collect human interaction
data from FLOSS communitiessuch as the tree of commits, IRC channels, mailing
lists, and issues tracker. The next step will be the improvement of this
version by CSSL researchers to support qualitative and quantitative data
analysis and visualization for this and future research on FLOSS
ecosystems.

Finally, \textbf{Phase IV} will triangulate the resulting conceptual maps with
findings from the data mining and the participant and non-participant
observation in the case of the Linux project. Also, we will discuss the
advantages, challenges and limitations of this multi-method research design to
help future research in the choice of appropriate research methods.

\section{Significance of the Study}
\label{sec:contributions}

Multivocal literature review is a powerful tool to summarize both
state-of-the-art and -practice, including negative results, current
discussions, and emerging research topics in both software engineering and
software industry. The comparison between its findings will \textbf{produce a more
accurate map of gaps and conflicts between theory and practice in the
comprehension of the FLOSS phenomenon and its contemporary development}, improving the
conduction and relevance of future FLOSS research in software engineering.

In addition to aligning state of the art and enriching it with the state of the
practice, we expect to \textbf{improve the mechanisms that support research on FLOSS
ecosystems}. On the one hand, we are developing a platform for collecting and
mining data from different means of interaction in a community. In the scope
of this study, the platform will allow a quantitative and qualitative
analysis of the attributes that characterize the development model of a
given project considering the role of social aspects in the construction of
the community workflow of a FLOSS project. It will also provide an open
database to support future investigations in the area.
%
On the other hand, this work raises questions about the criteria used in the
selection of methods for research in FLOSS ecosystems, since the divergent
discourse of academic researchers and practitioners is sharp. Hence, we
reinforce in this study the relevance of giving voice to both formal and
informal productions and adopting in-depth research methods able to
capture the ``what" and the ``why" of practice. In summary, we plan to
\textbf{a combination of qualitative and quantitative
research strategies that could boost FLOSS ecosystems research}.

\section{Manuscript Structure}
\label{section:organization}

This manuscript consists of three more chapters. Chapter \ref{cap:bkg} presents a history
of the FLOSS phenomenon (history, movement, and related development model) and
the Linux kernel (both as an operating system kernel and a FLOSS project).
Chapter \ref{cap:researchMethods} describes the research methods selected to guide this study,
discussing advantages, challenges, and specificities. Finally, Chapter \ref{cap:remarks}
presents the current research stage and the work plan. In addition, preliminary
productions are attached to this document to facilitate understanding of some
activities undertaken.
