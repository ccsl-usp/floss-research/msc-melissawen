# Systematic Literature Review Protocol

We considered Grey Literature any online publication not peer reviewed. The term ‘grey literature’ is often used to refer to reports published outside of traditional commercial publishing. (Cochrane)

## Research Questions

RQ1. How do software engineering studies describe the current Linux development model?

* What attributes do they use to characterize the Linux development model?
  * Do they compare this model to the Bazaar model?
* What research methods do they use to collect and analysis the Linux development characteristics?
* How do they usually bridge the theory with the practice?

## Data Source

1. Journal (Name of journal; Years searched; Any issues not searched)
1. Peer-reviewed Conference Proceedings (Title of proceedings; Name of conference (if different); Title translation (if necessary); Journal name (if published as part of a journal)

## Database

> In an attempt to perform an exhaustive search Brereton et al. [5] identified seven electronic sources of relevance to Software Engineers. (Guideline IST)

From which we chose:
1. IEEExplore
1. ACM Digital library:
1. ScienceDirect (www.sciencedirect.com)
1. Springer (https://link.springer.com/)
1. SCOPUS
1. Google scholar (scholar.google.com) [For double check]


> However, it may also be necessary to consider SpringerLink to access journals such as Empirical Software Engineering and Springer Conference Proceedings

Also, specific sources could be considered:
* Add name of individual journals (J) and conference proceedings (C) sources that had published papers on the topic.
  * Peer-reviewed magazines - papers with more than 4 pages (IEEE Software (?))


## Search string

| Linux                | Development | Model      |
|----------------------|-------------|------------|
| Linux Kernel         | ()          | Structure  |
| Linux Project        |             | Practices  |
| Bazaar               |             | Rules      |
|                      |             | Process    |
|                      |             | Community  |
|                      |             | Premisses  |
|                      |             | Management |
|                      |             | Ecosystem  |

After testing:

Full text: "Linux kernel" AND ("Development Model" OR "community Practices" OR "development Practices" OR "community Practices" OR "Development Process")

## Interval time

* 2009 - 2019+ (10 years)

## Sample selection

### Inclusion and exclusion criteria

| Inclusion criteria | Exclusion criteria |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| Published in a peer-reviewed venue, including magazines, conferences, and journals | Publications not peer-reviewed (workshops)|
| Available in English | Unavailable in English |
| Most current version of the document | Duplicated or there is a extended (updated) version of the document |
| Any study that discribed the Linux kernel development: reporting practices and/or presenting statistics and/or discussing management and rules inside the project and/or studying the project development process and/or its community | Did not contain any reference of development practices of Linux project |
|  | Merely discusses technical issues / coding / new features or studies that do not provide empirical evidence |

### Search process:

1. Test the efficiency of the search string by evaluating the papers retrieved by the tool.
1. Apply the search string in the search tool of each database.
1. Export results to an spreadsheet.
1. Remove duplication in the same database and cross-database.
1. Screen title.
1. Evaluate the relevance/quality of each venue.
1. Remove papers from venues with low relevance to the FLOSS and SE area.
1. Screen abstract.
1. Search for snowballing references.

### Selection process:

1. Use (at least) two people working independently to determine whether each study meets the eligibility criteria.
1. Whener possible, screening of titles and abstracts to remove irrelevant reports (should be done in duplicate by two people working independently but it is acceptable that this initial screening is undertaken by only one person).
1. Two people working independently are used to make a final determination as to whether each study considered possibly eligible after title / abstract screening meets the eligibility criteria based on the full text of the study report(s).
1. It is important that at least one author is knowledgeable in the area under review, it may be an advantage to have a second author who is not a content expert.
1. Disagreements about whether a study should be included can generally be resolved by discussion. 
1. A single failed eligibility criterion is sufficient for a study to be excluded from a review. In practice, therefore, eligibility criteria for each study should be assessed in order of importance
1. Pilot test the eligibility criteria on a sample of reports
1. The selection process must be documented in sufficient detail to be able to complete a flow diagram and a table of ‘Characteristics of excluded studies’
