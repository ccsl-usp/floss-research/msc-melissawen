# Grey Literature Review Protocol

We considered Grey Literature any online publication not peer reviewed. The term ‘grey literature’ is often used to refer to reports published outside of traditional commercial publishing. (Cochrane)

## Initial steps

1. Searching for publications about grey literature review in software engineering
2. Select key database to grey literature FLOSS research

## Research Questions

RQ1. How do FLOSS development communities describe the current Linux development model and their ties to the Bazaar model?

* What attributes do they use to characterize the Linux development model?

## Data Source

### Textual

1. Foundations and Project Webpages
1. Industry-oriented magazines
1. Books
1. Experienced Developers Blogs
1. Whitepapers
1. Mailing-list

### Audio-visual

1. Movies
1. Videos on Youtube
1. Podcasts
1. TED Talks

Obs.: Contact experts: Antonio Terceiro (?), Dan Vetter (?), Jonathan Corbet (?), Nelson Lago (?)

## Database

### Textual

1. Targeted websites (recommendations from FLOSS experts; Locating organization via Google): 
  * https://opensource.org/
  * https://www.fsf.org/
  * https://www.kernel.org
  * https://www.linux.com
  * LWN.net
  * https://kernelnewbies.org/
  * https://www.linuxfoundation.org/
  * https://www.linuxinsider.com
  * http://planet.kernel.org/
2. Industry-oriented Magazines:
*  https://www.wired.com/
*  http://www.drdobbs.com/
*  https://www.linuxformat.com/
*  http://www.linux-magazine.com/
*  https://www.linuxjournal.com/
*  https://opensourceforu.com/
*  https://www.networkworld.com
3. Books:
* Free as in Freedom
* Producing OSS
* Understanding the Open Source Development Model
4. Experienced Developers Blogs
* http://www.linux-magazine.com/Online/Blogs
* https://blog.ffwll.ch/
* http://www.kroah.com/log/
* https://sage.thesharps.us/
5. Whitepapers:
* http://www.findwhitepapers.com/technology
* https://www.bitpipe.com/
6. Google advanced search
7. GL databases
* http://www.opengrey.eu
* https://arxiv.org/
8. Consultations with experts: Antonio Terceiro (?), Daniel Vetter (?), Jonathan Corbet (?), Nelson Lago (?)
9. Snowballing references and backlinks: https://majestic.com

### Audio-visual

1. https://google.com
1. Movies: Revolution OS, The Code (Documentary),  Pirates of Silicon Valley, The 5 Keys to Mastery
1. https://www.youtube.com
1. Podcasts: papolivre.org; Sunday Morning Linux Review (http://smlr.us/); Free as in Freedom (http://faif.us/); https://twit.tv/shows/floss-weekly; KernelPodcast(http://www.kernelpodcast.org/);
1. https://www.ted.com

## Search string

| Linux                | Development | Model     |
|----------------------|-------------|-----------|
| Linux Kernel         | Business    | Structure |
|                      | ()          | Scheme    |
|                      |             | Rules     |
|                      |             | Process   |
|                      |             | Guide     |
|                      |             | Community |
|                      |             | Culture   |
|                      |             | ()        |


## Hashtags

* Linux
* Linux Kernel
* Linux Project

## Interval time

* 2008 - 2018 (10 years)

## Sample selection

### Inclusion and exclusion criteria

| Inclusion criteria | Exclusion criteria |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| Published online by practitioners or researchers in the FLOSS area | Published by enthusiasts who have not participated in any FLOSS-related projects or any FLOSS research. |
| Available in English | Unavailable in English |
| Most current version of the document | There is a new (updated) version of the document |
| Describes the development of Linux kernel: reporting practices and/or presenting statistics and/or discussing management and rules inside the project and/or studying the project development and/or its community | Did not contain any reference of development practices of Linux project |
|  | Merely discusses technical issues / coding / new features |

### Search process:

1. Search on Google for relevant websites and contact experts to create a list of targets
1. When a search tool is available on the selected database, apply the search string
1. Otherwise, search by hand relevant contents
1. Export results to an spreadsheet
1. Remove duplication
1. Highlight title appeared relevant and analyze abstract, when available
1. Author profile: experience, role in FLOSS project, active or not
1. Publisher history/profile
1. Full text Reading
1. Snowballing references

### Selection process (from [Cochrane cap 4](https://training.cochrane.org/handbook/current/chapter-04)):
1. Use (at least) two people working independently to determine whether each study meets the eligibility criteria.
1. Whener possible, screening of titles and abstracts to remove irrelevant reports (should be done in duplicate by two people working independently but it is acceptable that this initial screening is undertaken by only one person).
1. Two people working independently are used to make a final determination as to whether each study considered possibly eligible after title / abstract screening meets the eligibility criteria based on the full text of the study report(s).
1. It is important that at least one author is knowledgeable in the area under review, it may be an advantage to have a second author who is not a content expert.
1. Disagreements about whether a study should be included can generally be resolved by discussion. 
1. A single failed eligibility criterion is sufficient for a study to be excluded from a review. In practice, therefore, eligibility criteria for each study should be assessed in order of importance
1. Pilot test the eligibility criteria on a sample of reports
1. The selection process must be documented in sufficient detail to be able to complete a flow diagram and a table of ‘Characteristics of excluded studies’