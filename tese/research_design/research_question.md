# Research Questions

RQ1. How do software engineering studies and FLOSS development communities describe the current Linux development model and their ties to the Bazaar model?

RQ1.1. What attributes do they use to characterize the Linux development model?

RQ1.2. Do the academia and the FLOSS development community use the same attributes?


# Schedule

* 01/08 - 30/09: Grey Literature
* 01/10 - 15/10: NIER - ICSE
* 15/10 - 31/10: Conceptual Maps
* 01/11 - 20/12: Systematic Literature Review
* 06/01 - 20/01: Conceptual Maps
* 20/01 - 31/01: Analysis Results
* 03/02 - 15/04: Participant Observation
* 15/04 - 15/05: Data Analysis
* 15/05 - 15/06: Crossing Findings
* 15/06 - 30/07: Writing Thesis


 
