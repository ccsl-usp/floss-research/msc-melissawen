%% ------------------------------------------------------------------------- %%
\chapter{Background}
\label{cap:bkg}

%% ------------------------------------------------------------------------- %%
\section{Free/Libre Open Source Software}
\label{sec:floss}

The Free Software and Open Source movements have in their bases the opposition
to established policies and models of the software development industry of their times.
While the Free Software movement was in opposition to the proprietary software
industry's dominance, the Open Source advocated the superiority of a bazaar-like
development over the hierarchical model typically used in proprietary
software projects. After more than 30 years of the Free Software movement and
20 years of Open Source, academia lacks a complete understanding of FLOSS
development's possible models. Hence, revisiting the history of Free and Open
Source Software is essential to uncover loose ends in academic investigations.

%% ------------------------------------------------------------------------- %%
\subsubsection{Free Software Movement}
\label{sec:freeMov}

The first steps of the Free Software movement were given in 1983 when Richard
Stallman announced the GNU project. The project's goal was to build a
Unix-compatible operating system to be made available for
free~\citep{stallman_1983,stallman_gnu}. In Stallman's view, the existence of a
free operating system would strengthen the cooperative development of computing
communities~\citep{gnu_history} that had been weakened by the dominance of the
private software industry. This movement was consolidated two years later with
the publication of the GNU Manifesto in Dr. Dobb's Journal of Software Tools
and the Free Software Foundation (FSF) creation. FSF supports the GNU project
and aims to promote the development and use of free software and ensure the
freedom to copy, study, modify, and redistribute software. In 1989, the
same project created the ``GNU General Public License'' (GPL)~\citep{gpl}, a
copyleft scheme that eliminates usage restrictions in software and other kinds
of intellectual production.

Only in 1992, with the Linux kernel incorporation, the GNU project became a
complete operating system. The existence of a free operating system and the
GNU/Linux project's success led to the growth of software projects under the
GPL license and its volunteer communities, which, in turn, led to increased
commercial interest in its development process.

%% ------------------------------------------------------------------------- %%
\subsubsection{Open Source Movement}
\label{sec:osMov}

In 1997, through a set of observations made in the Linux kernel community and
lessons learned from his experiences with other software projects, Eric Raymond
presented the essay ``The Cathedral and the Bazaar''. In this essay,
\citet{Raymond1999} defines two antagonistic software development models: the
cathedral model and the bazaar model. Projects classified as cathedrals would
be those projects that follow a hierarchical model of development, with long
periods between the release of new versions and the users' little participation
in the software construction process. In opposition to that, \citet{Raymond1999}
claims that the bazaar model, like Linux, is open to receiving any user's contribution, with quick
releases and assuming the risk of failures and bugs. For him, the bazaar model
was like a horde of anarchists competing and surpassing the hierarchical form
of closed software development.

Th term Open Source was used by Raymond to propagate a depoliticized and
ideologically unbiased concept of free software projects. Raymond defined that
the bazaar development model, or Linux model, would be the model to be followed
by open-source software. With an initial set of Linux development
characteristics and an apparent dissociation of moral issues, adopting a bazaar
model intensified commercial actors' participation in Open Source projects.
\textit{``Release early and often. Delegate everything you can. Be open
to the point of promiscuity''} was a premise that won the developers' sympathy
and users of the projects and companies. They could see advantages in
innovation and improvement of their products by opening the code for
contributions.

Bruce Perens edited the Debian Free Software Guidelines to form the Open Source
Definition and registered a certification mark on the term Open Source. He
transferred the mark's ownership to Raymond, and they together formed the Open
Source Initiative, \textit{``an organization exclusively for managing the Open
Source campaign and its certification mark''}~\citep{dibona1999open}.

Raymond's essay has become a landmark
for the free software universe until now. While on the one hand, the essay
sowed the investigation, understanding, and implementation of good practices of
OSS development. On the other hand, the defense of the term open source as an
apolitical synonym of free software disturbed some practitioners and generated
a rupture between them. Stallman published an online article stating that Open
Source software misses the point of Free software~\citep{stallman_2019}. Perens
stated the concept of Free Software was outdated when he opted for Open Source.
However, some years later, he explained that Open Source was coined to promote
the concept of Free Software to business people and regrets that the Open
Source Initiative deprecated Richard Stallman and Free Software. For him,
\textit{``Open Source licenses and Free Software licenses are effectively the
same
thing.''}\footnote{\url{https://perens.com/2017/09/26/on-usage-of-the-phrase-open-source/}}

%% ------------------------------------------------------------------------- %%
\subsection{FLOSS Development Model} \label{sec:FLOSSDevMod}

According to \citet{Mockus2002}, FLOSS processes can produce high-quality and
widely deployed software; however, the exact means responsible for the success
are frequently debated. Meritocracy, cooperative spirit, and code transparency
are all part of this, but they are not enough to explain a project's daily
routine and how they resolve conflicts~\citep{Fogel2005}.

We can consider the Cathedral and Bazaar models presented by \cite{Raymond1999}
as a preliminary initiative to characterize freely licensed software
development models. When observing free software projects of that time, Raymond
separated in two sets the characteristics found by him in a personal project
and projects like Linux and GCC, as summarized below:

\textbf{The Cathedral}

\begin{itemize}
	\item \textbf{Development team:} a group of few people, well-trained
		and working in isolation.
	\item \textbf{Release process:} does not include beta versions. Do not
		release a version of the software until it has as few bugs as
		possible so as not to undermine the user's patience. Uses very
		long release intervals which increase the user expectations for
		a perfect version.
	\item \textbf{Debugging:} takes a long time. A bug is a complicated and
		profound phenomenon. Only a small group of developers can solve
		them.
\end{itemize}
\textbf{The Bazaar}
\begin{itemize}
	\item \textbf{Development team:} composed of co-developers available
		via the internet. Contributors having different schedules and
		approaches, motivated by ego satisfaction, perception of
		constant improvements of the software coming from their works,
		personal needs, or love. Users can also be co-developers,
		helping to find, suggesting fixes, and improving code faster.
		Contributions are made by people who are sufficiently
		interested in using the software, learning how it works, trying
		to find solutions to the problems encountered, and actually
		producing a reasonable fix. The coordinator needs to be able to
		communicate well and attract good people. The developers are
		selfish agents who try to maximize their productivity and,
		during this process, spontaneously auto-organizing the
		development workflow.
	\item \textbf{Release process:} Make software versions available as
		soon as possible and as often as possible. Perfection is
		achieved when the code produced is not only efficient but also
		as simple as possible. Coding, enhancement, and debugging is
		parallelizable when the mode of development is based on rapid
		interactions.
	\item \textbf{Debugging:} Maximize the number of person-hours debugging
		and developing even if it costs code instability and
		introduction of an intractable bug. Given a sufficient number
		of testers and developers, almost all problems will be
		characterized quickly (by someone), and the solution will be
		evident to others. A bug is a superficial phenomenon (easily
		visualized), or is brought to the surface quickly when a
		version is available to several observers. Debugging is
		parallelizable and does not require much coordination, just a
		useful reference. In practice, debug rework (duplication) is
		rarely a problem, and this rework is reduced with increasing
		launch frequency. More users find more bugs because they can
		stress software in different ways. There is always the option
		to use a previous (stable) version if a critical bug is found
		in the current tree (unstable).
\end{itemize}

According to \citet{Raymond1999}, starting a project already with the shape of
a bazaar is not trivial since someone needs a plausible promise to create a
community, i.e., something executable and testable for this community.
\citet{Raymond1999} states that more important than being brilliant, a project
coordinator should be able to identify promising ideas from other people's
projects. Coordinators should know when to restrict the innovations and
complexities in favor of code robustness and objectivity. Moreover, they should
understand that the free software community values its reputation. Therefore,
they should pressure people not to initiate development efforts that are not
worth moving forward.

After more than two decades, the FLOSS development based on geographically
distributed communities has evolved in an OSS 2.0 due to the growing
participation of commercial actors~\citep{Fitzgerald2006}. The author
characterizes both FLOSS in its origin as its current version and points out
possible gaps between the focus of OSS
research and the OSS phenomenon itself. Also, investigations that mine data
from the contributions' flow of different projects point to the FLOSS
dissociation of the bazaar model. 

\citet{Fogel2005} states that as a project gets old, it tends to move from a
benevolent dictatorship model to an openly democratic system with group-based
governance. This model would then be a ``consensus-based democracy'', where the
group works in consensus most of the time, and there is a formal voting
mechanism. Finally, after metamorphosing into a group-based system, a project
rarely moves back.

Literature reviews supplemented and updated this new shape of FLOSS
development. \citet{Scacchi2006} reviews empirical studies of FLOSS projects to
identify practices, processes, dynamics, and other socio-technical concerns
involved in these ecosystems and research opportunities in this field of study.
The author also points to the relevance of resources and capabilities
supporting this kind of development. \citet{Osterlie2007} conduct a critical
review of the formal literature to investigate, through discourse analysis, how
software engineering research has treated \textit{open-source} as a homogeneous
phenomenon~\citep{Osterlie2007}. This analysis demonstrates various points where
academia has built biased concepts about the practice and advocates for the
heterogeneity and multidisciplinarity of \textit{open-source} development
(OSSD).

\citet{Kon2011} discuss how the wealth of information available in FLOSS
project artifacts can benefit Software Engineering research and
education. They also evaluated where the Brazilian Software Engineering
research community stands with regard to FLOSS.
\citet{Crowston2012} reviewed commercial publications between 1999 and
2006 related to empirical research in the development of FLOSS. The purpose of
the paper is to synthesize what academia has known and unknown about the
process and public/private practices involved in the FLOSS development.
Ultimately, \citet{Steinmacher2015} selected 20 studies that provide empirical
evidence of newcomers' barriers when contributing to a FLOSS project.
They classified 15 barriers into five categories and also problems according
to their three origins.

Considering the diversity of FLOSS projects, \citet{Capiluppi2003} examined
almost 400 projects to find FLOSS project properties.  Their work extracted
generic characterization (project size, age, license, and programming
language), analyzed the average number of people involved in the project
(developers, subscribers, and core team), the community of users, modularity,
and documentation characteristics.

Some FLOSS practices overlap with agile values regarding individuals and
interactions, working software, customer collaboration, responsiveness to
change, and software project management~\citep{Beck2010, Gandomani2013}.
Besides, FLOSS projects rely on self-organized teams and team-wide shared and
coherent goals~\citep{Adams2009, Tsirakidis2009}.  \citet{Warsta2003} found
differences and similarities between agile development and FLOSS practices.
Open communication, project modularity, the users' community, and fast response
to problems are just a few of the FLOSS ecosystem practices
\citep{Capiluppi2003, Warsta2003}. The authors argued that FLOSS development
might differ from agile in their philosophical and economic perspectives; on
the other hand, both approaches share similar work definitions.

We have also reported the reproducibility of sound FLOSS development practices
in a software development collaboration~\citep{WenSPB}. We explained FLOSS
community development standards, such as developers being also users of the
system under development.  FLOSS project is usually divided into fronts of
self-organized development teams, identifying among developers the leadership
roles. They use open means of communication to code review, track activities,
and organically document the project, processes, and technical decisions.
Finally, project stakeholders participate in the development routines and
workflow through their employees.

%% ------------------------------------------------------------------------- %%
\section{The Linux Kernel}
\label{sec:kernel}

Linux is a monolithic kernel of the family of Unix-like operating systems. It
is not a complete Unix operating system since it does not include filesystem
utilities, windowing systems, desktop environment, text editors, and compilers.
Unlike Unix, the Linux kernel is under the GNU General Public License version 2
only (GPL-2.0), with an explicit syscall
exception\footnote{\url{https://www.kernel.org/doc/html/latest/process/license-rules.html}}.
This license protects the code of commercial ownership and benefits it with
developing the userspace project GNU. Also, the project is open and available
to anyone to study~\citep{Bovet2005}.

According to \cite{Tanenbaum}, Linux is a rewrite of MINIX, with numerous ideas
from this microkernel project and other new features. The Linux kernel uses
modules to achieve microkernel advantages. A module is an object
file whose code is linked to the kernel at runtime. This object code usually
implements features at the kernel's upper layer~\citep{Bovet2005}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\textwidth]{figure/linux_arch.png}
	\caption{An Overview of the Linux Architecture}
        \label{fig:linux_arch}
\end{figure}	

The Linux kernel is divided into three levels, as shown in Figure 
\ref{fig:linux_arch}: the top-level implements basic functionalities of the System
Call Interface, such as read and write; the architecture-independent kernel code is
in the middle, and the architecture-dependent code is in the
bottom-level~\citep{ibm_kernel}.

%% ------------------------------------------------------------------------- %%
\subsection{The Linux Project}
\label{sec:linuxproject}

The Linux kernel project currently has almost 25 million lines of code and much more
than 1,500 active developers from more than 250 different companies (or from no
company). Its development process differs significantly from proprietary
development methods, with a community-based scheme, a patch-oriented
development process, and a time-based release process.

%% ------------------------------------------------------------------------- %%
\subsubsection{Linux History}
\label{sec:linuxhistory}

Linux is an operating system kernel that began to be written by Linus Torvalds
in 1991~\citep{torvalds_1992}. It was initially private, but in 1992
Linus made it free software, enabling the GNU project to adopt this kernel and
thus creating the GNU/Linux operating system. The first stable version of
Linux (1.0) was released in 1994, with about 165,000 lines of code~\citep{Tanenbaum}. Due to the
full participation of the GNU community - concerned with the compatibility of
the system with the stable version of the kernel - and the dynamics of
geographically distributed volunteers coordinated by Torvalds, the project grew
to such an extent that, five years later, version 2.2.0 already had 1.8 million
lines of code~\citep{ibm_kernel}. 

Throughout its three decades, the Linux creator, Linus Torvalds, has repositioned the
project regarding the Free Software and Open-Souce movements. In 2007, Torvalds
positioned itself against the adoption of GPLv3~\citep{torvalds_2007}, created by
the Free Software Foundation. More recently, in a TED of 2016, he stated that he
was never really concerned with following an open source methodology or free
software policies~\citep{ted_2016}. He just opened the code for feedback and created
methods and tools (such as Git) that made it possible to shape development in
the most comfortable possible way for him. Finally, in a letter recently sent to the
kernel project mailing list in 2018, Torvalds licenses himself for a brief period from the
coordination of Linux due to strong disagreements with the community regarding
his way of conducting the project~\citep{torvalds_2018}. Greg Kroah-Hartman
took over Torvalds' coordination responsibilities and, after a month, Torvalds
returned to Linux development~\citep{linus_return}.
At that time, the controversial code of conflicts was replaced by a new code
of conduct as a symbol of a welcoming and inclusive community that cares about
its developers.

%% ------------------------------------------------------------------------- %%
\subsubsection{The Project Structure}
\label{sec:linuxstructure}	


The kernel code is organized as a set of subsystems\footnote{\url{https://lwn.net/Articles/844539/}} 
such as process scheduler, memory management, device driver infrastructure,
networking, and filesystems~\citep{kernel_newbies} (depicted by Figure
\ref{fig:linux_sub}). 
Usually, each subsystem has designated maintainers. A maintainer is the project role
responsible for managing and accepting contributions into a specific
subsystem code repository.
The kernel project uses the Git source management tool to coordinate contributions.
Contributions are formatted and sent to the Linux kernel project, as a patch.
A patch is a text document describing the differences between two different
versions of a source tree.
These contributions (or patches) are grouped according to a particular need or
interest. This collection of patches are arranged in a tree. And each tree works
as a place of a specific development.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\textwidth]{figure/linux_subsystems.pdf}
	\caption{Linux Subsystems}
        \label{fig:linux_sub}
\end{figure}

A contribution sent to a subsystem does not go directly to the mainline tree of Linux. 
Each subsystem maintainer has a version of the kernel source tree, which enables
the maintainer to track patches. The chain of subsystem trees guides the flow of
patches to Torvalds' source tree~\citep{linux_corbet}. 
 
At the beginning of a development cycle, a ``window'' is opened to
merge the code of changes accepted by the maintainers of a given subsystem into
the mainline kernel. Changes in each subsystem trees are merged to
Torvalds' source tree via pull requests.
After this time, Torvalds declares the window as closed and
releases a first intermediate release~\citep{kernel_process}. 
There is a somewhat informal process to ensure that a change is in the quality
and relevance appropriate for the official repository. This evaluation is taken
by top-level maintainers and also by Torvalds. Sometimes, he takes
special attention to some particular patch; however, in general, he trusts
maintainers will not send bad code.

Linux development processes extensively rely on human evaluation and subjectivity.
Notwithstanding the building of a chain-of-trust, the community has raised
questions about Torvalds' behavior~\citep{torvalds_2018}, the hierarchies, and
also the processes of review and acceptance of changes~\citep{danvetter_post}.
These conflicts reinforce the relevance of socio and behavioral aspects for
examining the Linux kernel development model. Moreover, the high-dependence on
human judgment intrinsically puts informalities, imprecisions, and
unpredictability to the project development and deliverables.

This background motivates us to investigate the characteristics of Linux kernel
development in light of the project community's human aspects. Bearing in mind
FLOSS phenomenon transformations, we aim to capture an up-to-date picture of roles,
rules, and responsibilities that support the development community. Therefore, we
present in the next chapter the research methods chosen to conduct our comprehensive
 research in the Linux kernel development project.

