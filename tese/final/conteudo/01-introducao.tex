%% ------------------------------------------------------------------------- %%
\chapter{Introduction}
\label{cap:introducao}

Free/Libre Open Source Software (FLOSS) development has been the subject of
investigation in academia and industry. Their interest stems from the popularity of
various software projects and their successful use of peculiar management
and training practices to conduct a high-quality collaborative, geographically
distributed, community-based software development effort.

The search for effective and efficient ways
of producing computer programs is at the heart of the Software Engineering (ES)
discipline. Nevertheless, a certain detachment from the practice observed throughout the history of
academic studies on FLOSS may led many investigations to carry myths and
superficial understanding of its phenomenon and an outdated view of the
development model of one of the largest, most famous, and most consolidated
projects in the history of FLOSS phenomenon, the Linux kernel.

The Linux kernel development is connected in many ways to the history of the
free software and open-source movements. Also, few FLOSS projects have managed
to survive three decades and still have a pulsating development as the Linux kernel.
Its characteristics inspired software development models. Its long history,
large community, and the interest of software engineering studies in
understanding its practices provide a great wealth of open-access sources of
information available for investigation. 

The primary objective of this thesis is to describe the current development
community of the Linux kernel project, considering community publications and
academic studies. Our purpose is to capture community attributes, challenges
and concerns presented by practitioners, and compare the coverage of them by
ES investigation. We also aim to identify research techniques and data sources
able to mitigate the distance between what is investigated by
academia and what is observed in practice on the development of the Linux
kernel -- the project that inspired one of the first initiatives to define a
FLOSS development model, the essay ``The Cathedral and the Bazaar''~\citep{Raymond1999}.
We\footnote{I often use ``we'' to describe the
results of the collaborative process that involves my advisors and/or other
researchers at the FLOSS Competence Center of the University of São Paulo
(CCSL); the use of ``I'' is specific to work performed individually.}
consider that the real-world gaps in many studies on this kind of software are
not caused by a lack of methodological discipline and rigor in an investigation
but rather by the absence of research approaches that use methods to shed light
on the practice and endorse scientific findings.

In short, this investigation proposes to extract the attributes used to
characterize the Linux kernel development community from the state-of-the-art and
state-of-the-practice. We examine the outputs from academia and the Linux kernel community
publications to capture the attributes used to describe the Linux development
community model, including project management characteristics, organizational
structure, workflow, and decision-making process. We also participated in the
daily community to explore other nuances of the project characteristics, making
industry-as-laboratory. From this mapping, we deliver a comprehensive study on
the Linux kernel project's development community in light of the social and
behavioral aspects of a distributed, community-based workflow.

\section{Problem outline}
\label{sec:problem}

At the end of the nineties, \citet{Raymond1999} described two supposedly
antagonistic FLOSS development models through an essay named ``The Cathedral
and The Bazaar''. These models were based on his observations of the Linux
kernel\footnote{\url{https://www.linuxfoundation.org/projects/linux}}
development and lessons learned during his own software
project, the \textit{fetchmail}. The seminal essay aimed to disseminate an
apolitical and unbiased term to define the
development model of free software projects, the term \textit{open-source}. 
Consequently, a new wave inside the free software communities emerged and brought along with it
firms interested in the benefits of these open-source practices that enable
innovation, technical support from an external community, and ``many-eyes''
assisting the software development~\citep{Crowston2012}.

Companies' participation in FLOSS development projects has caused changes in
their communities, development processes, and business strategies.
As a result, these elements lost several free software characteristics and
became more mainstream and viable~\citep{Fitzgerald2006}.
The development of many FLOSS projects is now supported by large companies,
either directly or through contributions from paid employees~\citep{Lakhani2003}. 
Thus, FLOSS projects are less and less bazaar-like as strategic
planning becomes essential. At large, it became a public product produced by the private
initiative~\citep{Crowston2012}. 
Other transformations came from the project evolution in terms of structure
and technologies for supporting distributed management and development, such as
version control systems, instant messengers, and authentication keys.
In short, FLOSS development has followed the web-based pace of globalization and
innovation.

In the third decade of existence, the Linux kernel project also has
transformed itself. It steadily grows in terms of source-code size, developer team
size, and variety of markets. With the popularity of Android smartphones, the
kernel is used by a large portion of the global population. Its current
development model is in a different scenario and presents other aspects of
those on which the Bazaar model was based. A core group of developers takes
most of the reviews and is also exclusively responsible for approving code
changes, passed by a chain-of-trust to Linus Torvalds' official repository.
Therefore, the risk of code instability and intractable bug insertion on the
mainline may not vary with the number of testers and developers. This aspect
resembles the Cathedral model's design and contrasts with the so-called
``Linus' Law'' defined by Raymond as ``Given enough eyeballs, all bugs are
shallow''.

Many Software Engineering studies already dissociate the Linux kernel development
from the bazar-like model.
For \citet{Rigby2014}, Linux is currently managed by a dictatorship where its
review process has an average of two reviewers, and 44\% of patches sent to the
project are ignored and never merged to the code.  However, for
\citet{Fogel2005}, code ``forkability'', present in the Linux project, is why
there are no real dictators in FLOSS projects, but a particular type of
dictatorship, the ``benevolent dictator'' model. In this model, the dictator
does not have a strict hold on the project, letting things work themselves out
and usually making final decisions only if there is no consensus.
For \citet{Lindberg2014}, the Linux kernel follows a cathedral model, but
\citet{Shaikh2017} argue that four governance types coexist in the Linux kernel
development: autocracy, oligarchy, federation, meritocracy. This unclear
definition reveals that understanding the Linux kernel development is an
ongoing issue in academic investigations.

The annual increase in the number of publications of empirical work on FLOSS
demonstrates the growing interest in the subject~\citep{Crowston2012}.
Notwithstanding the apparent consonance between industry and academia on the
absorption of FLOSS practices or products in software development projects,
\citet{Osterlie2007} reveal a lack of understanding of the FLOSS phenomenon in
academic works, exposing the homogeneous and biased point of view of many
studies and their findings. For the authors, the distancing of software
engineering studies from the practice gives a chance to reproduce imprecision.
Another aggravating factor of this imprecision is the initial and
non-comprehensive use of non-formal materials, such as blog posts,
videos, whitepapers, and websites. These materials are commonly produced by
FLOSS practitioners in their day-to-day activities and are defined
by \citet{Scacchi2007} as \textit{Software Informalism}. 

Many studies look at some artifacts produced by the community. In general, they
examine lines of code, commits, and emails. However, although Software
Engineering publications widely use the term community-based to describe the
FLOSS development model, academic works do not always examine
what the community has said about the project of which it is a part. These
mismatches led many software engineering studies to ignore the diversification
of development forms in the FLOSS ecosystem and transformations on the Linux
kernel development model after two decades since
``The Cathedral and The Bazaar''. 

Given this scenario, for this thesis, we delimited the scope of some terms as
follows:

\begin{itemize}
\item \textit{Free Software}: refers to the Free Software movement, formalized by Richard Stallman in 1983
\item \textit{Open Source}: refers to the Open-Source movement, a movement to pitch a concept of ``free software'' development viable in the business world, as initially explicited by the Cathedral and Bazaar essay.
\item \textit{FLOSS, an acronym for Free / Libre and Open Source Software}: refers to both free and open software ecosystems. This term will be extensively used in discussions of practices and methods used by developers in the freely licensed software community.
\item \textit{FLOSS Phenomenon}: when the topic in discussion involves characteristics of the Free Software and Open-Source movements
\item \textit{FLOSS Development}: when the subject is limited to the workflow and set of techniques used in the production of this kind of software.
\end{itemize}


\section{Research Questions}
\label{sec:researchquestions}

The Linux kernel project has a protagonism defining what we currently know as
the bazaar-like FLOSS development model. Besides, academia and industry have
shown the relevance of using FLOSS development practices for innovation and
software quality. Accordingly, this research primary goal is to map the
attributes that describe the contemporary Linux development community model
considering the state-of-the-art and -practice of FLOSS development
characteristics. We collect and analyze data from FLOSS community publication
and Software Engineering studies on Linux kernel development, comparing our
findings. I conduct a participant-observation in the Linux kernel community to
reinforce convergences and address possible divergences between what is
claimed by academia and observed in daily work practice.

The following research questions will guide our investigation:

\textit{\textbf{RQ1. How do software engineering studies and Linux community publications describe the current Linux development community model?}}

\textit{ RQ1.1. What attributes practitioners use to characterize the Linux development community? What are the current social and organizational challenges from the community perspective?}

\textit{ RQ1.2. Do Software Engineering studies already cover these topics?}

\textit{\textbf{RQ2. What research techniques can be used to examine a FLOSS project through its community publications?}}

\textit{\textbf{RQ3. What are the possible gaps and opportunities for academic research in FLOSS development topics?}}

The Linux project's selection takes into account the role played by this
project in the Free Software and Open-source movements. Also, much of the FLOSS
model's academic understanding is based on assertions about the Linux project
development model -- or Bazaar model.
Describing the contemporary characteristics of the
Linux project's workflow and community, in contrast with its traditional
characterization, will produce an updated view of the roles, rules, and
restrictions present in the Linux development model, one of the major
projects in FLOSS history.

\section{Research Design}
\label{sec:researchdesign}

A variety of research methods supports FLOSS ecosystem studies, and the case
study is, without doubt, the most common method. On the other hand, multi-method
investigations are not largely used and are often designed to incorporate
interviews into case studies, surveys, and field studies~\citep{Crowston2012}.
Researchers are interested in elucidating factors and features of FLOSS
development that increase the chances of a software project succeeding. 
According to \citet{Fogel2005}, it is not difficult for a FLOSS project to
achieve technical success; however, after initial success, it needs a
social foundation and a robust developer base to handle the growth or loss of
skilled workforce.

Over the last two decades, the Linux kernel project grew in size, diversity,
and maturity. Besides examining the human interaction networks intrinsic of
community-based development, this study intends to benefit from the wealth of
information present in the non-academic publications. We aim to expand the
Linux kernel ecosystem's academic understanding by providing a socio-technical
picture of the contemporary Linux kernel development community.

We conducted a multivocal literature review~\citep{Garousi2018} to describe the Linux development
community model from the academia and community perspectives.
When reviewing multivocal literature, we examine documents from both grey literature and academic studies on FLOSS development.
To the best of our knowledge, no single study has focused on identifying FLOSS
research opportunities in the Linux kernel development model by comparing
characteristics mapped in Software Engineering studies with those presented in
FLOSS community publications. Moreover, conducting a multivocal literature
review in the Linux kernel project reveals mismatches and misunderstandings
between academia and industry communities about one of the most prominent and
valuable projects for the FLOSS phenomenon and history.

We also collected and analyzed qualitative data from my participant-observation
in the Linux kernel project community. We verified findings from this ethnographic
method with those obtained in the multivocal literature review to discuss a
potential misleading characterization of the Linux kernel development model
and Software Engineering research opportunities in FLOSS.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{figure/workflow2.png}
	\caption{Practical and Theoretical Study Workflow}
        \label{fig:workflow}
\end{figure}

Figure\ref{fig:workflow} summarizes the research strategies, methods, expected
outcomes, and the current stage of the investigation.
This research is divided into four phases: the first phase of the acquaintance
of data sources and research methods; the second phase of multivocal literature
review; the third phase of the ethnographic case study; and the fourth and final
phase of confirming results by triangulation.
Each phase is detailed below.

\textbf{Phase I} is a warm-up on the studies of the FLOSS phenomenon and its
development model. At this stage, we conducted a preliminary analysis of
traditional and non-traditional literature regarding elements commonly used to
describe FLOSS development models and community workflows. These introductory
materials support the construction of a search string for the following steps.
Another initial step is the introduction of the observer in the Linux kernel
community. In this phase, I had to sharpen my code skills and started dialogues
with community members to collect data by participant observation.
Due to the steep learning curve for becoming a kernel developer and having some
project participants in our research group, we chose to start this preparatory
phase of participant observation as soon as possible.

\textbf{Phase II} summarizes the state-of-the-art and -practice via a
multivocal literature review. The grey literature review identifies attributes
commonly used by the FLOSS community to describe the Linux kernel development
community. The systematic review of traditional literature and, finally, the
combination of findings enables creating a comparative mind map of the
contemporary Linux kernel development community from academic researchers' and
practitioners' perspectives.
Examining one data source has the inherently potential of bringing bias to the
review of the other. Taking into account this risk, we chose to start the
multivocal review by grey literature due to the greater proximity of these
materials with the practitioners' routine and the low systematized review of
them in FLOSS research.

\textbf{Phase III} strengthens Phase II's findings through an in-depth
ethnographic case study on the Linux kernel community. At this stage, we focus
on understanding people, project culture, and their related social and work
practices to shed light on mismatched understandings between academic
researchers and practitioners. I participate in the community's daily
life to capture undocumented characteristics of the Linux kernel development.
As an observer, I also identify misleading information and shallow approaches for
some subjects of software engineering studies that become research
opportunities on FLOSS.

Thus, we design an ethnographic case study of the Linux kernel project. Data
collection follows a qualitative method of participant-observation, where an observer becomes a
member of the community under investigation, participating in daily tasks and
different situations for members' interaction. On the one hand, I
volunteered in FLUSP\footnote{\url{https://flusp.ime.usp.br/}}, a group focused on
FLOSS project development at the University of São Paulo.
I performed necessary activities of communication and contribution
to participate in a Linux kernel subsystem's development process,
the Industrial I/O\footnote{\url{https://www.kernel.org/doc/html/latest/driver-api/iio/index.html}}.
In the continuity of the investigation, I joined another Linux subsystem, the Linux DRM for GPU
drivers\footnote{\url{https://www.kernel.org/doc/html/latest/gpu/introduction.html}}, taking a longer participant-observation that included the
performance of different community roles: a volunteer newcomer; an independent
developer for the Google Summer of Code
program\footnote{\url{https://summerofcode.withgoogle.com/}};
co-maintainer of a subsystem driver; and co-mentor of a project for
Outreachy\footnote{\url{https://www.outreachy.org/}}, a paid internship program.
Driver maintainership involves clerical and development tasks, such as,
reviewing contributions to the driver, keeping driver development code up-to-date,
bug-fixing, synchronized driver code to subsystem updates, etc.
Mentorship also involves many activities, such as evaluating candidates, designing the internship projects,
guide newcomers through drivers and subsystem code, explaining concepts, introducing to the subsytem community, review contributions.
Therefore, this multirole experience also enables virtual interactions with other
community members to comprehend their behaviors, communication styles, and
development process perceptions with mine.

Finally, \textbf{Phase IV} triangulates the findings from the GLR, SRL and
participant and non-participant observation in the case of the Linux project.
Also, we discuss the advantages, challenges and limitations of this
multi-method research design to help future research in the choice of
appropriate research methods.

\section{Claimed Contributions}
\label{sec:contributions}

This thesis aims to investigate features of the Linux kernel community in
an up-to-date view of the Linux kernel development model. From this goal, we claim
that this work delivers four primary original contributions:

\textbf{C1. A comprehensive characterization of the contemporary Linux kernel
development community.} The systematic grey literature review
and content analysis resulted in a mind map of main concepts used by Linux
kernel community members to describe the development of its common-source
project. We grouped the concepts in main subjects regarding characteristics of
the community as a unit, members concerns and natural and legal persons
participation. \textbf{C1.1 A map of potentiallly misleading information and gaps
between theory and practice in the contemporary Linux kernel development
community.} We design a multi-method investigation to map convergencies in
theory and practice on the Linux kernel development community.  We combine and
compare findings from reviewing software engineering studies and community
publications to provide an up-to-date description of the project development
community. We participated in the daily life of the community to observe
undocumented features. Consequently, we produce a comprehensive and critical
mapping of misleading information and gaps in the Linux kernel development
model's academic understanding. Filling these gaps improves the investigative
design and increases the relevance of future FLOSS research.

\textbf{C2. Community characteristics and social-technical nuances that
shapes a FLOSS project development.}
Many FLOSS projects are supported by a community of
developers. In the Linux kernel project, the development model is defined by
concepts that describe its release process, artifacts, software-product, and
community. Discussion on the Linux kernel structures, rules, and roles used to
organize a community of contributors is dispersed in Software Engineering
studies. We thus provide a rationale in which community aspects set the pace of
the current Linux kernel development and are the basis of the sustainability of
the project. Therefore, we argue that FLOSS development should be evaluated
from a social perspective, where social aspects impact development processes,
growth, and continuation of a project. We also explored the advantages of
participant observation to analyze the content and discuss the multivocal
review results in one of the most successful projects in the FLOSS phenomenon.

\textbf{C3. Guidelines to examine FLOSS projects through its community
publications.} We systematize the entire process of reviewing and analyzing content
from grey literature produced by the Linux kernel community. The methods,
challenges, and adaptations reported here can guide future grey literature
review on FLOSS and Software Engineering studies. From our lessons learned in
this process, FLOSS Researchers can also anticipate issues and design a
protocol to prevent bottlenecks when dealing with a large amount of content,
usually poorly structured.

\textbf{C4. A combination of research strategies that could boost research on
FLOSS ecosystems.} We resort to different systematic literature reviews and
qualitative methods to summarize state-of-art and -practices in the Linux
kernel development model. The multivocal literature review is a powerful method
to include negative results, current discussions, and emerging research topics
in software engineering and the software industry. It also captures
practitioners' point of view that, combined with a participant-observation,
provides a comprehensive understanding of FLOSS project characteristics and
transversal social aspects of FLOSS development. We use data from community
publications, software engineering studies, and participant-observation to
expand our research on the Linux kernel ecosystem with concepts from theory,
practice, and development trenches. To date, no other Software Engineering
studies on FLOSS have systematically reviewed community publications from FLOSS
projects and, moreover, combined findings with a systematic literature review.

C1 and C1.1 resulted from answering RQ1 and RQ1.2.
C2 is a consequence of answering RQ1.1. 
C3 is related to the RQ2.
Finally, RQ3 covers C4 and C1.1.

\subsection{Publications in the area of FLOSS Development}
\label{sec:papers}


We have published a total of five conference and journal papers based on this
research or covering diverse themes related to topics of this thesis. 

\textbf{P1.} Wen, M., Leite, L., Kon, F., Meirelles, P. (2020). Understanding FLOSS through community publications: strategies for grey literature review. Proceedings of the ACM/IEEE 42nd International Conference on Software Engineering: New Ideas and Emerging Results, ICSE-NIER 2020.

\textbf{P2.} Wen, M., Siqueira, R., Lago, N., Camarinha, D., Terceiro, A., Kon, F., Meirelles, P. (2020). Leading Successful Government-Academia Collaboration Using FLOSS and Agile Values. Journal of Systems and Software. Volume 164.

\textbf{P3.} Wen, M., Meirelles, P., Siqueira, R., Kon, F. (2018). FLOSS project management in government-academia collaboration. Open Source Systems: Enterprise Software and Solutions, OSS 2018. (Best Paper Award)

\textbf{P4.} Siqueira, R., Camarinha, D., Wen, M., Meirelles, P., Kon, F. (2018). Continuous delivery: Building trust in a large-scale, complex government organization. IEEE Software, 35(2). 

\textbf{P5.} Meirelles, P., Wen, M., Terceiro, A., Siqueira, R., Kanashiro, L., Neri, H. (2017). Brazilian public software portal: An integrated platform for collaborative development. Proceedings of the 13th International Symposium on Open Collaboration, OpenSym 2017.


\section{Thesis Structure}
\label{section:organization}

This thesis consists of five more chapters. Chapter \ref{cap:bkg} presents a
history of the FLOSS phenomenon -- history, movement, and related development
model -- and the Linux kernel both as an operating system kernel and a FLOSS
project. Chapter \ref{cap:researchMethods} describes the research methods
selected to guide this study, discussing advantages, challenges, and
specificities. Chapter \ref{cap:linuxdevmodel} proposes strategies for conducting Grey
Literature Reviews on FLOSS based on guidelines from related works on Software
Engineering literature review. We present the decisions taken and
adaptations made to examine and analyze FLOSS community publications
with different formality levels. This process resulted in a comprehensive mind
map describing the Linux kernel development community. We also discuss the
community concerns regarding process scalability, member behavior, and
project subsistence from the practitioner's perspective.
Chapter \ref{cap:twoperspectives} presents related works on the Linux
development model characteristics as systematic literature review
findings. We compare results here to those from the grey literature review.
%Besides related works on the background and methods adopted in this research,
% esse captiulo complementa/aprofunda outros trabalhos relacionados
We include a third point
of view from a participant-observation to discuss the characteristics and
concerns of the Linux kernel community.
We also map and discuss the matches and mismatches of the attributes considering the
theory and practice of Linux kernel development.
Finally, Chapter \ref{cap:conclusion}
concludes this research. We answer our research question and discuss opportunities for future
Software Engineering research on FLOSS from the gaps mapped by our multivocal literature review.

