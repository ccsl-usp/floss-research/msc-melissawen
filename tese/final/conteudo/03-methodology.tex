%% ------------------------------------------------------------------------- %%
\chapter{Research Methods and Design}
\label{cap:researchMethods}

According to \citet{Osterlie2007}, software engineering literature has
presented little about what is performed by practitioners, what activities they
do, and how often they happen.
More recently, \citet{Aniche2019} have claimed that ``It is fundamental that the
real-life experience of practitioners influences the work of researchers in the
field'' of software engineering.
To fill the gap between practice and theory, researchers should move from a
research-and-transfer model to an industry-as-laboratory approach. With this
concern in mind, this work proposes a multi-methodological investigation,
adopting the multivocal literature review and the ethnographic case study. For
the multivocal literature review, we systematically examined grey and formal
literature. Also, we performed an ethnographic case study through online
participant observation in the Linux kernel project. The use of multiple
methods enables to update the academia and practitioners' perception of the
characteristics of the Linux kernel, the ecosystem that inspired the FLOSS
development model.

In this chapter, we introduce the research methods that guide this
investigation. We discuss the advantages and challenges of each of them. We
also present guidelines for the use of each approach in software engineering
investigations.

%% ------------------------------------------------------------------------- %%
\section{Research Strategies}
\label{sec:researchstrategy}


%\todo{Não manjo desses paranauê, mas acho que engenheiros de soft querem que
%tudo seja feito formalmente (o que, como bem sabemos, tem lá seus problemas).
%Mas o que você está dizendo aqui é outra coisa: que a \textit{pesquisa} em
%engenharia de soft sempre se baseia apenas em documentação formal. Se isso é
%verdade (ou na medida em que isso é verdade), o problema não é só com FLOSS; a
%análise de qualquer projeto que não seja super-formal vai dar merda}.

Searching for information only in formal bibliographic references does not fit well into
the universe of FLOSS. A FLOSS ecosystem usually has a community around its
project, organically producing materials that describe how, when, and why a
particular change occurred in the source code or the development flow. This
production provides activity logs, documentation of discussion, and
decision-making present in the developer's day-to-day. \citet{Scacchi2007}
define as \textit{Software Informalism} the information resources and artifacts
used by participants to describe, prescribe, or proscribe what happens in a
FLOSS project. They are informal, comparatively easy-to-use, and publicly
accessible narratives such as source code, artifacts, online repositories, with
substantial size, diversity, and complexity not previously available globally.
Online and non-academic publications can complement these materials to broaden
the academic understanding of how FLOSS project developers interpret the
development model of which they are part.

Besides the specificity in the production of informal narratives published
openly on the Internet, FLOSS development studies must also consider the
pillars that sustain developers' community for a given project. Trust and
responsibility among project participants are invisible social control
resources that support the open and complex development of
FLOSS~\citep{Scacchi2007}. Describing how contributors interpret the
organization and workflow requires research methods capable of explaining the
rationalities from an insider's point of view and puts the social, cooperative,
and human aspects of software engineering practice at the
center~\citep{Sharp2016}. Finally, empirical studies on FLOSS are growing and
expanding the scope of what we can observe, analyze and learn about these
software systems and their development. Traditional research methods join new
techniques for mining repositories and analysis of the socio-technical
processes and environments which support FLOSS projects~\citep{Scacchi2010}.

Considering the research questions enunciated in Section
\ref{sec:researchdesign}, we have a mix of question types.
\textit{\textbf{RQ1. How do software engineering studies and Linux community publications describe the current Linux development community model?}} is exploratory and
descriptive. To answer subquestion \textit{What attributes practitioners use to
characterize the Linux development model? What are the current project
challenges and concerns from the community perspective?}, we conduct a
\textit{grey literature review} to construct a comprehensive, up-to-date view
of the Linux kernel development model considering the FLOSS community
perspective. After, we analyze the selected documents using quantitative and
qualitative content analysis techniques to identify the attributes used to
characterize the Linux development community. For the subquestion (\textit{Do
Software Engineering studies already cover these topics?}), we chose the
\textit{multivocal literature review} with a comparative content analysis
approach to assess the convergence between what is studied in academia and what
is experienced in the practice of the Linux development community.

\textit{RQ1} also has an ethnographic nature because it concerns itself with
the social and behavioral aspects of groups of people in the means of
interactions. Community and project diversity constrain the statistical
generalization of results found in case studies of FLOSS projects. However, the
ethnographic approach communicates a detailed picture that enables analytical
generalizations. Therefore, we include a participant-observation to explore the
nuances from the daily work on the Linux kernel development process and capture
the ``what'' and ``why'' of practices from different qualitative sources of
data. We collect qualitative data through online participant observation in two
Linux subsystems and non-participant observation of informants.

\textit{\textbf{RQ2. What research techniques can be used to examine a FLOSS project through its community publications?}}
is exploratory and descriptive. It involves deploying new methods to review
non-academic literature and incrementally adapting the data collection and data
analysis processes. Therefore, we describe the methods and adaptations to
conduct the grey literature review on FLOSS, using the Linux kernel project as
a case study.
Finally, we approach \textit{\textbf{RQ3. What are the possible gaps and opportunities for academic research in FLOSS development topics?}} by choosing specific
content analysis methods to systematize concepts from FLOSS community
publications and highlighting which of these concepts were already covered by
studies in commercial publishers. We also compared findings from
participant-observation and analyze with those found in
the literature review to illustrate how to find gaps and build bridges on
theory and practice understanding of a FLOSS project development.

%% ------------------------------------------------------------------------- %%
\subsection{Multivocal Literature Review}
\label{sec:multivocal}

A Multivocal Literature Review (MRL) is a form of systematic literature review
that includes inputs from academic peer-reviewed papers and sources from the
Grey Literature~\citep{Garousi2018}. It aims to provide summaries of both the
state-of-the-art and state-of-the-practice in a given area.

\subsubsection{Systematic Literature Review} \label{subsec:slr}

Systematic Literature Review (SLR) is a research methodology used to synthesize
and evaluate the available evidence on a focused topic~\citep{Biolchini2007}.
It follows a rigorous methodology of research results to support evidence-based
guidelines for practitioners~\citep{Kitchenham2009}. The evidence knowledge
allows researchers to capture gaps and concept conflicts addressed by new
studies in the area~\citep{Melo2013}.

In contrast to ad-hoc literature reviews, a systematic review follows a
sequence of well-defined, strict steps following a protocol planned before
execution. The whole SLR process must be documented, explicitly defining
methodological steps, collection strategies, focus, research question, and
intermediate results. 

According to \citet{Cochrane2008}, a systematic review should have the
following characteristics:

\begin{itemize}
\item A set of clearly stated goals with predetermined inclusion and exclusion
criteria for studies;
\item An explicit and reproducible methodology;
\item The use of systematic research to reach all studies that fit the
eligibility criteria;
\item An evaluation of the validity of the findings of the selected studies,
including the risk of bias; and
\item A systematic and synthesized presentation of the characteristics and
results of the selected studies.
\end{itemize}

Although SLR has become more prevalent in empirical software engineering, many
of these studies use medical standards to guide reviews~\citep{Kitchenham2013}.
\citet{Budgen2006} proposed a three-phase review process for Software
Engineering: (1) planning the review; (2) conducting the review; (3) reporting
the outcomes from the review. \citet{Biolchini2007} developed a similar
three-phase template based on protocols and systematic review guidelines in the
medical field to guide the planning and execution of SLR in Software
Engineering. This template handles each phase of the SLR process as detailed
below:

\begin{itemize}
\item \textit{Review Planning:} defines question focus; question quality and
amplitude, i.e., problem, question, keywords, Population-Intervention-Comparison-Outcome (PICO) components, and others semantics
specificity; Source Selection (criteria definition, studies languages, sources
selection, source and references evaluation); Studies Selection (inclusion and
exclusion criteria; studies types; procedures for selection).
\item \textit{Review Execution:} Selection Execution (reports primary studies
selection); Information Extraction (describes extraction criteria, results, and
resolution of divergences among reviewers).
\item \textit{Result Analysis:} summarizes and analyzes results using
statistical methods.
\end{itemize} 

According to \cite{Kitchenham2007}, the main advantages of SLR are:

\begin{itemize}
\item Low probability of bias due to the use of a well-defined methodology
(although not exempted to the prejudices of primary studies);
\item Informs the effects of a phenomenon across a wide range of settings and
empirical methods;
\item In the case of quantitative studies, SLR enables the combination of data
using meta-analysis techniques.
\end{itemize}

The SLR primary challenge is that it requires more effort than a
non-systematic review. Some additional problems arise in software engineering;
for example, digital libraries lack mechanisms to perform complex queries,
papers omit information, and their abstracts are poor~\citep{Kitchenham2013}.

\subsubsection{Grey Literature Review}
\label{subsec:glr}

According to \citet{Rothstein2005}, Grey Literature (GL) is any source of
information produced from academia, industry, business, and government,
published in print or electronic formats, but commercial publishers do not
control it. It is composed of sources of data not found in the formal
literature, such as blogs, videos, white papers, and web-pages. 

Alongside commercial and academic publications, GL can be used to broaden the
understanding of how FLOSS developers interpret the environment of which they
are part. Despite the informality of GL, it brings advantages of its own.
Studies with negative or null results are easier to find in GL than in
peer-reviewed academic literature, enabling a more critic perspective, with a
potential reduction of bias and visualization of more balanced
evidence~\citep{Paez2017}. GL is also a leading source for identifying topics
and gaps not yet covered by academic literature. It also enables investigating
more up-to-date, and emerging information since academic studies incur long
publication delays due to the peer-review process.

Although it is still a nascent trend, a few software engineering reviews have
already included GL material~\citep{Bailey2007,travassos2016SLR, Soldani2018}.
Nevertheless, we argue that GL material use is even more fruitful in FLOSS
research, given the vast amount of public information resources produced by the
FLOSS communities. In such a scenario, we claim that researchers should
investigate such resources before taking the time of FLOSS contributors with
surveys or interviews~\citep{Garousi2016}, especially considering that
\emph{i)} most of the needed information is already available with rich details
and \emph{ii)} top contributors usually do not have much time for extensive
interviews.

Combining systematic review protocols with GLR helps shed light on critical
industrial and practitioners concerns not yet mapped by software engineering
studies~\citep{Soldani2018}. However, the use of GLR also has challenges.
Although systematic seview guidelines, such as \citet{Cochrane2008}, recommend
the inclusion of this literature, a precise method for implementation is not
available. Thus, GLR still faces difficulties due to the large volume of
information and the lack of indexing standards and vocabulary~\citep{Godin2015}.

\citet{Godin2015} present an application of systematic review methods in GLR,
where information sources include GLR databases, customized Google search
engines, targeted websites, and consultation with experts. They use title
and source organization of documents for eligibility assessment and study
selection. Finally, they search for a pre-defined set of data on each study
selected.

\subsubsection{Conducting a Multivocal Literature Review}
\label{subsec:mlr}

A multivocal literature review (MRL) could improve software engineering research
relevance by analyzing input from practitioner literature. \citet{Garousi2018}
present guidelines to include GLR in SLR and conduct a multivocal literature
review. The proposed MRL process is composed of five phases:

\begin{itemize}
\item \textbf{Search process:} 
\begin{itemize}
\item Defining search string and/or using snowballing techniques.
\item Where to search: search formally-published literature via broad-coverage
abstract databases and full-text databases; search GLR via a general
web search engine, specialized databases, and websites, contacting individuals
directly or via social media, and/or reference lists and backlinks, and perform
an informal pre-search to find different synonyms for specific topics.
\item Stopping criteria: theoretical saturation; effort bounded; evidence
exhaustion;
\end{itemize}
\item \textbf{Source selection:} combine inclusion and exclusion criteria for
GLR with quality assessment criteria; in the source selection
process of an MLR, one should ensure a coordinated integration of the source
selection processes for GLR and formal literature.
\item \textbf{Study quality assessment:} apply and adapt the criteria authority
of the producer, methodology, objectivity, date, novelty, impact, as well as
outlet control, for study quality assessment of grey literature.
\item \textbf{Data extraction:} design data extraction forms; use systematic
procedures and logistics, such as maintaining ``traceability'' links, and
extracting and recording as much quantitative/qualitative data as needed to
sufficiently address each research question; 
\item \textbf{Data synthesis:} many GLR sources are suitable for qualitative
coding and synthesis; argumentation theory can be beneficial for data
synthesis; quantitative analysis is possible on GLR databases such as
StackOverflow.
\end{itemize}

\citet{Crowston2012} have stated that the use of secondary data was not a research
mechanism widely adopted in research on FLOSS development.
Moreover, although SLR is a
well-established investigative method, with steps and restrictions already
defined in several fields of study, MLR, in turn, is an emerging technique,
especially in the area of software engineering. Its conduction still requires
adaptations for the variety of publication types on a particular topic under
investigation. To perform an MLR, the reviewer should deal with the lack of
robust search engines (developing customized search mechanisms), the diversity
of the size, quality, and structure of the retrieved documents, and the
challenge of textual and natural language analysis.

%% ------------------------------------------------------------------------- %%
\subsection{Ethnographic Case Study}
\label{sec:ethnography}

Ethnography is a research method designed to describe and analyze the social
life and culture of a specific social system~\citep{Edmonds2013}. This
approach's central tenet is to understand values, beliefs, or ideas shared for
a group understudy from the members' point of view~\citep{Sharp2016}. For this,
the ethnographer needs to become a group member, observing in detail what
people do and learning their language, social norms, rules, and artifacts.

In software engineering, ethnography is an appropriate method of research when
one wants to understand people, culture and the social and work practices
associated with them~\citep{Sharp2016}. From an empathic perspective, it can
explain the logic of practice from insiders' view and, thus, capture what
practitioners do and why they do. Considering people's behavior an integral
part of software development and maintenance~\citep{Seaman2008}, ethnography in
software engineering can strengthen investigations of social and human aspects
in the software development process since the significance of these aspects of
software practice is already well-established.

The ethnographic case study focuses on examining a real case in some cultural
group, delimited by time, place, and environment~\citep{Edmonds2013}. Its design
is characterized by the intensive and holistic description and analysis of a
particular social reality. It is best suited for investigations interested in
exploring a group's activities rather than shared patterns of group behavior.
Ethnography imposes an orientation of the investigative view on the symbols,
interpretations, beliefs, and values that integrate the socio-cultural
dimension of a community's dynamics~\citep{Sarmento2011}. In this way, an
ethnographic case study must present an investigative design that employs
convergent methods with such orientation. \citet{Sharp2016} defines four main
characteristics of ethnographic research in empirical software engineering:

\begin{itemize}
\item \textbf{The members' point of view:} focus on the informants' point of
view, understanding what is, or is not, important, and painful for the
informant.
\item \textbf{The ordinary detail of life:} collect several types of data about
different aspects of their informants' work and keep ``open'' for new
possibilities. 
\item \textbf{The analytical stance:} provide an analysis of the results
explaining how this evidence is, or is not, relevant for a particular purpose.
\item \textbf{Production of ``thick descriptions'' for academic accountability:}
provide results well rooted in meaningful aspects.  Comparing and contrasting
data, their aggregation, and ordering it in logical sequences to structure the
knowledge.
\end{itemize}

The design of an ethnographic study must also consider five dimensions: (1)
participant or non-participant observation; (2) the duration of the field
study; (3) space and location; (4) the use of theoretical underpinnings; (5)
the ethnographers’ intent in undertaking the study.

\subsubsection{Data Collection} 

Three data collection methods are primarily used in ethnographic research:
participant observation, interviews, and documentation
analysis~\citep{Sarmento2011}. In this work, we collect Linux kernel project
data by examining the community-produced documents and the
participant-observation in two Linux subsystem communities.

\textbf{Participant and Non-Participant Observation} 

Data from direct observation contrasts information obtained through interviews
and questionnaires. The reason for the differences is that humans do not always
do (or will do) what they say they have done, especially when it involves their
reputation. In this case, the participant observation method makes it possible
to explain the meaning of the observed fact's experiences through the observer
of the fact experienced~\citep{robson_realresearch} and allow the informant to
judge what is essential rather than what she thinks is essential.

An ethnographer spends her time working, discussing, participating, and living,
interacting with informants. She becomes a member of the experimental group,
interpreting what is happening around her. Hence, in addition to sensitivity,
the observer needs the personal skills necessary to conduct participant
observation. In a FLOSS community, the software engineer has the facility to
assume participant-observer's role and provide valuable information and
practical consequences for software practice. Also, informants of the same
culture and living in similar conditions generally reduce the time required to
complete an ethnographic study~\citep{Sharp2016}.

According to \citet{Seaman2008}, a considerable part of software development
work is not documented because it occurs within the developer's mind. Software
developers find it easier to reveal the processes present in their thoughts
when communicating with other software developers, making this communication a
valuable opportunity to observe the development process. In this context, a
data collection method is the continuous observation of a developer, recording
all interactions with the other group members through notes or journals. This
journal should be kept confidential throughout the study to preserve the
observer's total freedom of writing. Each note should include the location,
time, and participants of the observation, the discussions that took place, the
events during the observation, and the tone and feelings involved in the
interactions. As the observer becomes more familiar with the group studied, the
notes gain a wealth of detail.

\subsection{Data Analysis and Crossing Information}

Qualitative data are those in which information is expressed through figures or
words, whereas categories or numbers represent quantitative
data~\citep{Seaman1999}.  An approach to extracting quantitative variable's
values from qualitative data is the process of \textit{coding}. Coding makes it
possible to get more precise and reliable quantitative data and quantify
subjective information to perform some quantitative or statistical analysis. To
avoid loss of information in this transformation, the researcher needs to be
aware of the variations of terminology in describing the phenomena, the
different ratings for the same subject, and the margin of reliability between
them.

Three processes describe the coding analysis~\citep{Edmonds2013}:
\begin{enumerate}
\item \textit{Open coding} identifies categories of information about the phenomenon being examined.
\item \textit{Axial coding} involves taking each one of the categories from the open coding and exploring it as a core phenomenon.
\item \textit{Selective coding} systematically relates the core category to other categories. 
\end{enumerate}

In this work, we consider distinct approaches for qualitative content analysis:
conventional, directed, and summative~\citep{hsieh2005}. They differ by how the
initial codes are developed. When an initial coding scheme exists, it is
revised and refined by interpreting the contextual meaning of specific terms or
developing additional codes.
\begin{enumerate}
\item \textit{Conventional content analysis} derives code categories during content analysis and directly from the text data.
\item \textit{Directed content analysis} starts from initial codes defined by a previous theory or relevant research findings and improves them during data analysis.
\item \textit{Summative content analysis} starts by counting and comparing keywords derived from interest of researchers or review of literature. Additional analysis proceeds to interprete the underlying context.
\end{enumerate}

These different approaches also support crossing information.
Crossing and analyzing information obtained from different sources, different
types of data, or different collection methods allows performing a
Triangulation~\citep{Seaman1999}. It is considered the most powerful method of
``confirming'' the validity of conclusions. In the case study, triangulation is
essential to prevent the unilaterality in a piece of observation, a
testimonial, or a document that could compromise the understanding of
reality~\citep{Sarmento2011}. Different kinds of measurements provide repeated
verification; however, triangulation gives reliability rather than validity
information. This method also reveals inconsistency or even direct conflicts,
helping to elaborate our findings or initiate a new line of
thinking~\citep{Miles2015}.

The crossing of information obtained through the three data
collection methods mentioned above (GLR, SLR and Participant-Observation)
can provide a comprehensive and current view of the characteristics of the
Linux development model. 
Following our workflow (Figure \ref{fig:workflow}) and the methods
described here, the next two chapters describe the process of data collection
and analysis according to the research phase and the information source. We
present our findings from each investigative stage and how we combined them to
reach the study goals. Due to the lack of definition on grey literature
review methods, we also provide a set of strategies to examine FLOSS community
publications. Finally, we discuss our findings considering three perspectives:
academic studies, community publications, and development community participation.
