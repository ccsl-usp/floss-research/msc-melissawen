%% ------------------------------------------------------------------------- %% \\
\chapter{Getting Involved in the Linux Kernel Community}
\label{att:getinvolved}

A distributed,large community of users and developers surrounds the Linux kernel
project. The community is a place where users and developers can report
problems, obtain code review and testing and expert advice. Besides, it is the
gateway for changes to the kernel code~\citep{Love2010}.

To join any community, a new member needs to assimilate the workflow and some
basic rules of interaction. These information is available in the Linux kernel
Documentation (\url{https://www.kernel.org/doc/html/latest/process/howto.html}),
and in books, such as, the Linux Kernel Development of \citet{Love2010}.
Besides, a new member should know the community norms to
preserve a welcome environment. Members and new members must respect each other
and avoid negative behavior in the community, as mentioned in
\url{https://www.kernel.org/doc/html/latest/process/code-of-conduct.html}.
Moreover, due to the software's singularities, a newcomer should develop skills
to correctly change a piece of the system code.

\section{Training Activities - Development Environment Setup}

\subsection{Basic Setup - This Research Approach}

\textbf{Setup e-mail client:} This step consists of installing and configuring
an e-mail client that allows the use of the terminal to send contributions
managed by git version control system. The researcher selected
Neomutt\footnote{\url{https://neomutt.org/}} as the client.

\textbf{Join the IRC channel of the selected project:} Here is the place for
quick discussions and questions/answers with other active developers. For the
IIO subsystem, developers meeting in \#linux-iio on OFTC network.

\textbf{Clone the kernel git repository:} Because each subsystem maintainer has
a commit tree to accept works related to a particular subsystem, the
contributor must keep the selected subsystem maintainer tree up to date. The
git trees to contribute have a different location, and they are specified in
the MAINTAINERS file: \url{https://www.kernel.org/doc/linux/MAINTAINERS}

\textbf{Install a virtual machine for development:} it prevents that, by some
carelessness, the contributor damages its operating system. This care is
necessary because by modifying and compiling a Linux image, the developer can
replace the image of his/her system and insert a critical bug.

\textbf{Learning how to modify, compile, and install a Linux kernel image:}
consists of a series of commands and settings that need to be executed or
updated to perform the proposed work. I used the Kworkflow tool, a set of
scripts to automate commands executed in the daily development activities:
\url{https://github.com/kworkflow}

\subsection{Understanding the process of sending contribution}

\textbf{Learn the Linux kernel development process:} The Linux kernel
development process is well-described in the project documentation at
\url{https://www.kernel.org/doc/html/latest/process/2.Process.html}

\textbf{Learn the code style rules:} The project has code standardization rules
described at
\url{https://www.kernel.org/doc/html/latest/process/coding-style.html}.  The
community also developed tools to verify compliance with these rules. 

\textbf{Execute tutorials to introduce newcomers to the contribution flow:} The
Kernel Newbies website provides tutorials to foster newcomers' participation in
the Linux kernel community. These tutorials could be found at
\url{https://kernelnewbies.org/FirstKernelPatch}

\section{Getting involved in the IIO subsystem}

\textbf{Code clean-up:} Find the file of a drive. Run code check tools like
checkpatch.pl or Coccinelle. These tools reveal easy-level issues and guide a
newcomer to correct the code follow the code style rules.

\textbf{Exploring staging drivers:} Drivers in the staging tree are in the main
Linux kernel source tree, but, for technical reasons, it was still not merged
into the main portion of the Linux kernel
tree\footnote{https://lwn.net/Articles/324279/} Therefore, developers can
improve these drivers beyond code style. To develop substantial improvements, a
developer should search for the driver documentation. An example is a Datasheet
of the IIO-driver AD7150 from Analog Devices:
\url{https://www.analog.com/media/en/technical-documentation/data-sheets/AD7150.pdf}

\subsubsection{Contributions sent to the Industrial I / O subsystem}

\begin{table}[]
\centering
\resizebox{\textwidth}{!}{\begin{tabular}{  | m{2.5cm} | m{6cm} | m{2.5cm} | m{3cm} | m{2cm} | m{2cm} |  }
\hline
\textbf{1st Version Date} & \textbf{Commit message} & \textbf{Developer(s)} & \textbf{Kind of change} & \textbf{\# Versions} & \textbf{Status} \\ \hline
Apr 2, 2019 & staging: iio: frequency: ad9834: Remove unnecessary parentheses & Melissa Wen & Code style & 1 & Merged \\ \hline
May 3, 2019 & staging: iio: ad7150: organize registers definition & Melissa Wen & Improve readability & 1 & Declined \\ \hline
May 3, 2019 & staging: iio: ad7150: use FIELD\_GET and GENMASK & Melissa Wen & Improve maintanability & 2 & Merged \\ \hline
May 3, 2019 & staging: iio: ad7150: simplify i2c SMBus return treatment & Melissa Wen & Reduce verbosity & 2 & Merged \\ \hline
May 3, 2019 & staging: iio: ad7150: clean up of comments & Melissa Wen & Improve readability & 2 & Merged \\ \hline
May 18, 2019 & staging: iio: cdc: ad7150: create of\_device\_id array & Barbara Fernandes, Wilson Sales Melissa Wen & Create missing element & 1 & Merged \\ \hline
May 18, 2019 & staging:iio:ad7150: fix threshold mode config bit & Melissa Wen & Bug fix & 1 & Merged \\ \hline
Jun 14, 2019 & staging: iio: ad7150: use ternary operating to ensure 0/1 value & Melissa Wen & Remove idiom, Make operation consistent & 1 & Merged \\ \hline
\end{tabular}%
}
\caption{List of contributions sent to IIO Linux subsystem}
\label{tab:patcheslist}
\end{table}

\section{Anatomy and contribution flow: the case of [PATCH] staging:iio:ad7150: fix threshold mode config bit}

Proposing a code change involves a series of interactions. Each patch
submission improves the acquaintance with processes, standards established,
communication style among members, and roles played by each in code review and
merging process. The Linux kernel documentation already presents a Howto for
sending a code
contribution\footnote{\url{https://www.kernel.org/doc/html/latest/process/submitting-patches.html}}.

\subsection{Sending a contribution by e-mail}

In this section, we present a real-submission patch e-mail and point out
recommendations from this research experience. 

\textbf{Header Information}

In addition to submitting a contribution to the appropriate mailing list for
the related file, the recipient list must include the driver maintainers and
others who may be interested in reviewing the proposed changes.

\textit{Date: 18 de maio de 2019 22:04 \\
From: M... \\
To: L... \\
Cc: linux-iio@vger.kernel.org, devel@driverdev.osuosl.org, linux-kernel@vger.kernel.org \\}

\textbf{Change Description}

The commit message should also describe what is modified and the reasons for
this change and reference documents or tools that support the proposed change.
In this case, the developer cited the driver configuration description
presented in the driver datasheet.

\textit{``According to the AD7150 configuration register description, bit 7 assumes \\
value 1 when the threshold mode is fixed and 0 when it is adaptive, \\
however, the operation that identifies this mode was considering the \\
opposite values. \\
 \\
This patch renames the boolean variable to describe it correctly and \\
properly replaces it in the places where it is used.''\\}

\textbf{Special Tags}

For some cases, the developer must include tags already standardized.  The list
of tags is presented at
\url{https://www.kernel.org/doc/html/latest/process/submitting-patches.html#when-to-use-acked-by-cc-and-co-developed-by}
and
\url{https://www.kernel.org/doc/html/latest/process/submitting-patches.html#using-reported-by-tested-by-reviewed-by-suggested-by-and-fixes}
Tags give the credits for contributors involved in the code change, beyond the
author of the patch.

\textit{Fixes: 531efd6aa0991 ("staging:iio:adc:ad7150: chan\_spec conv + i2c\_smbus commands + drop unused poweroff timeout control.") \\}

\textbf{Commit Author(s)}
Signed-off-by: Melissa Wen $<$..@..$>$ \\

\textbf{Commit Diff}
--- \\
 drivers/staging/iio/cdc/ad7150.c | 19 +++++++++++-------- \\
 1 file changed, 11 insertions(+), 8 deletions(-) \\
 \\
diff --git a/drivers/staging/iio/cdc/ad7150.c b/drivers/staging/iio/cdc/ad7150.c \\
index dd7fcab8e19e..e075244c602b 100644 \\
--- a/drivers/staging/iio/cdc/ad7150.c \\
+++ b/drivers/staging/iio/cdc/ad7150.c \\
@@ -5,6 +5,7 @@ \\
  * Copyright 2010-2011 Analog Devices Inc. \\
  */ \\
 \\
+\#include $<$linux/bitfield.h$>$ \\
 \#include $<$linux/interrupt.h$>$ \\
 \#include $<$linux/device.h$>$ \\
 \#include $<$linux/kernel.h$>$ \\
@@ -130,7 +131,7 @@ static int ad7150\_read\_event\_config(struct iio\_dev *indio\_dev, \\
 \{ \\
        int ret; \\
        u8 threshtype; \\
-       bool adaptive; \\
+       bool thrfixed; \\
        struct ad7150\_chip\_info *chip = iio\_priv(indio\_dev); \\
 \\
        ret = i2c\_smbus\_read\_byte\_data(chip-$>$client, AD7150\_CFG); \\
@@ -138,21 +139,23 @@ static int ad7150\_read\_event\_config(struct iio\_dev *indio\_dev, \\
                return ret; \\
 \\
        threshtype = (ret $>$$>$ 5) \& 0x03; \\
-       adaptive = !!(ret \& 0x80); \\
+ \\
+       /*check if threshold mode is fixed or adaptive*/ \\
+       thrfixed = FIELD\_GET(AD7150\_CFG\_FIX, ret); \\
 \\
        switch (type) \{ \\
        case IIO\_EV\_TYPE\_MAG\_ADAPTIVE: \\
                if (dir == IIO\_EV\_DIR\_RISING) \\
-                       return adaptive \&\& (threshtype == 0x1); \\
-               return adaptive \&\& (threshtype == 0x0); \\
+                       return !thrfixed \&\& (threshtype == 0x1); \\
+               return !thrfixed \&\& (threshtype == 0x0); \\
        case IIO\_EV\_TYPE\_THRESH\_ADAPTIVE: \\
                if (dir == IIO\_EV\_DIR\_RISING) \\
-                       return adaptive \&\& (threshtype == 0x3); \\
-               return adaptive \&\& (threshtype == 0x2); \\
+                       return !thrfixed \&\& (threshtype == 0x3); \\
+               return !thrfixed \&\& (threshtype == 0x2); \\
        case IIO\_EV\_TYPE\_THRESH: \\
                if (dir == IIO\_EV\_DIR\_RISING) \\
-                       return !adaptive \&\& (threshtype == 0x1); \\
-               return !adaptive \&\& (threshtype == 0x0); \\
+                       return thrfixed \&\& (threshtype == 0x1); \\
+               return thrfixed \&\& (threshtype == 0x0); \\
        default: \\
                break; \\
        \} \\
--  \\
2.20.1 \\

\subsection{Receiving feedback on mailing-list}

\subsubsection{From maintainer}

\textbf{Header Information}

Date: 19 de maio de 2019 07:29 \\
From: J... \\
To: M... \\
Cc: ..., linux-iio@vger.kernel.org, devel@driverdev.osuosl.org, linux-kernel@vger.kernel.org, kernel-usp@googlegroups.com \\

\textbf{Comments inline}
On Sat, 18 May 2019 22:04:56 -0300 \\
Melissa Wen [...] wrote: \\
$>$ According to the AD7150 configuration register description, bit 7 assumes \\
$>$ value 1 when the threshold mode is fixed and 0 when it is adaptive, \\
$>$ however, the operation that identifies this mode was considering the \\
$>$ opposite values. \\
$>$  \\
$>$ This patch renames the boolean variable to describe it correctly and \\
$>$ properly replaces it in the places where it is used. \\
$>$  \\
$>$ Fixes: 531efd6aa0991 ("staging:iio:adc:ad7150: chan\_spec conv + i2c\_smbus commands + drop unused poweroff timeout control.") \\
$>$ Signed-off-by: Melissa Wen $<$..@..$>$ \\
 \\
Looks good to me.  Applied to the fixes-togreg branch of iio.git pushed out as \\
as testing-fixes for the autobuilders to see if they can find anything \\
we have missed. \\
 \\
Thanks, \\
 \\
Jonathan \\

\subsubsection{From an employee of the related device driver company}

\textbf{Header Information}
Date: 20 de maio de 2019 06:59 \\
From: A...	 \\
To: J... \\
Cc: ..., linux-iio@vger.kernel.org, devel@driverdev.osuosl.org, LKML $<$linux-kernel@vger.kernel.org$>$, kernel-usp@googlegroups.com, ... \\

\textbf{Comments inline}
On Sun, May 19, 2019 at 8:38 PM [...] wrote: \\
$>$ \\
$>$ On Sat, 18 May 2019 22:04:56 -0300 \\
$>$ Melissa Wen $<$..@..$>$ wrote: \\
\[...\] \\
$>$ $>$       threshtype = (ret $>$$>$ 5) \& 0x03; \\
$>$ $>$ -     adaptive = !!(ret \& 0x80); \\
$>$ $>$ + \\
$>$ $>$ +     /*check if threshold mode is fixed or adaptive*/ \\
$>$ $>$ +     thrfixed = FIELD\_GET(AD7150\_CFG\_FIX, ret); \\
 \\
nitpick: i would have kept the original variable name as "adaptive", \\
mostly for consistency. \\
"adaptive" is used in other places as well; \\
 \\
as i recall, the fix is just oneliner in this case: \\
 \\
- adaptive = !!(ret \& 0x80); \\
+ adaptive = !(ret \& 0x80); \\
 \\

\subsection{Receiving notification of merge}

This section present the e-mails received after the patch acceptance that
suggest the patch path from the subsystem maintainer tree
to other Linux kernel trees related to staging drivers.

\subsubsection{From Linux maintree maintainer}

Date: 17 de junho de 2019 17:32\\
From: gregkh@...	\\
To: m...\\
\\
This is a note to let you know that I've just added the patch titled\\
\\
    staging:iio:ad7150: fix threshold mode config bit\\
\\
to my staging git tree which can be found at\\
    git://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git\\
in the staging-linus branch.\\
\\
The patch will show up in the next release of the linux-next tree\\
(usually sometime within the next 24 hours during the week.)\\
\\
The patch will hopefully also be merged in Linus's tree for the\\
next -rc kernel release.\\
\\
If you have any questions about this process, please let me know.\\
\\
\\
From df4d737ee4d7205aaa6275158aeebff87fd14488 Mon Sep 17 00:00:00 2001\\
From: Melissa Wen $<$..@..$>$\\
Date: Sat, 18 May 2019 22:04:56 -0300\\
Subject: staging:iio:ad7150: fix threshold mode config bit\\
\\
According to the AD7150 configuration register description, bit 7 assumes\\
value 1 when the threshold mode is fixed and 0 when it is adaptive,\\
however, the operation that identifies this mode was considering the\\
opposite values.\\
\\
This patch renames the boolean variable to describe it correctly and\\
properly replaces it in the places where it is used.\\
\\
Fixes: 531efd6aa0991 ("staging:iio:adc:ad7150: chan\_spec conv + i2c\_smbus commands + drop unused poweroff timeout control.")\\
Signed-off-by: Melissa Wen $<$..@..$>$\\
Signed-off-by: Jonathan Cameron $<$..@..$>$\\
---\\
 drivers/staging/iio/cdc/ad7150.c | 19 +++++++++++--------\\
 1 file changed, 11 insertions(+), 8 deletions(-)\\

\subsubsection{From Linux version maintainer}

From: S[..]\\
Para: linux-kernel@vger.kernel.org, stable@vger.kernel.org\\
Cc: M..\\
From: Melissa Wen $<$..@..$>$\\

$[$ Upstream commit df4d737ee4d7205aaa6275158aeebff87fd14488 $]$\\

According to the AD7150 configuration register description, bit 7 assumes\\
value 1 when the threshold mode is fixed and 0 when it is adaptive,\\
however, the operation that identifies this mode was considering the\\
opposite values.\\
\\
This patch renames the boolean variable to describe it correctly and\\
properly replaces it in the places where it is used.\\
\\
Fixes: 531efd6aa0991 ("staging:iio:adc:ad7150: chan\_spec conv + i2c\_smbus commands + drop unused poweroff timeout control.")\\
Signed-off-by: Melissa Wen $<$..@..$>$\\
Signed-off-by: Jonathan Cameron $<$..@..$>$\\
Signed-off-by: Sasha Levin $<$..@..$>$\\
---\\
 drivers/staging/iio/cdc/ad7150.c | 19 +++++++++++--------\\
 1 file changed, 11 insertions(+), 8 deletions(-)\\
