# Mestrado USP

* **Tema:** Investigação da congruência de comunidades de software livre com os modelos de desenvolvimento Catedral e Bazar
* **Orientada:** Melissa Shihfan Ribeiro Wen
* **Orientador:** Fabio Kon
* **Co-orientador:** Paulo Meirelles


* [Plano de trabalho](https://gitlab.com/melissawen/mestradoUSP/wikis/plano-de-trabalho)

Para mais informações, veja página [Wiki](https://gitlab.com/melissawen/mestradoUSP/wikis/home)



