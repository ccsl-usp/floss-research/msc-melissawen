## Grounded Theory

* Key authors: Kathy Charmaz - Constructing Grounded Theory: Descreve etapa de codificação

* Discover and label variables and their relationships for generating a new theory
* Usado quando precisa-se estudar um fenômeno antigo num novo cenário.
* Um processo iterativo

### Etapas:
1. Coleta de dados: Fontes de dados
1. Dados e codificação: Categorias
  - Códigos são provisórios, sendo testados revisados e dispensados (se necessário);
1. Escrita de Memorandos
  - Interpretação dos códigos?
1. Teoria

### Dados:
* Transcrição de entrevista
* Nota de campo
* Documentos
* Texto de blogs e websites
* Vídeos
* Diagramas
* Etnografia

Daniel Cukier: A maturity model for software startup ecosystems
Fitzgerald

## Experimental Design

* Theory testing
* Lack of experimentation in Computer Science: Tichy(1993); Wainer (2009); Zelkowitz (1997)

What is required for a good validation?
Is it based only in quantifiable results?

Shari Pfleeger: Experimental design and analysis in Software Engineering



