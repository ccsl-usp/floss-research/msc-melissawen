# Python program to generate WordCloud 
  
# importing all necessery modules 
from wordcloud import WordCloud, STOPWORDS 
import matplotlib.pyplot as plt 
import pandas as pd
import spacy
from collections import Counter
import random

def grey_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
    return "hsl(0, 0%%, %d%%)" % random.randint(0, 60)
  
# Reads 'csv' file  
df = pd.read_csv(r"~/Mestrado/glr_content.csv", encoding ="utf-8") 
  
concepts = ''
nouns_s = []

stopwords = set(STOPWORDS)

nlp_s = spacy.load('en_core_web_sm')

# iterate through the csv file
df.Coding.fillna("", inplace = True)
for val in df.Coding: 
    
    # typecaste each val to string
    val = str(val) 
 
    # split the value 
    tokens = nlp_s(val)

    #remove stopwords
    tokens_s = [w for w in tokens if not (w.is_stop or w.is_punct or w.like_num or w.is_space or w.like_url or w.is_bracket or w.is_currency)]

    for word in tokens_s:
#        concepts = " ".join([concepts, str(word.lemma_)])+" "
        if word.pos_ in ["NOUN", "PROPN"]:
            concepts = " ".join([concepts, str(word.lemma_)])+" "
            nouns_s.append(word.lemma_)
#    concepts = " ".join(nouns_s)+" "

# 50 most common tokens
word_freq = Counter(nouns_s)
common_words = word_freq.most_common(200)

print("NOUN FREQUENCY:")
print(common_words)

wordcloud = WordCloud(width = 800, height = 800, 
                background_color ='white',
                max_words=300,
#                colormap="Oranges_r",
                stopwords = stopwords.update(["kernel kernel", "patch patch", "bit", "lot"]), 
                min_font_size = 10).generate(concepts) 
  
# plot the WordCloud image                        
plt.figure(figsize = (8, 8), facecolor = None) 
plt.imshow(wordcloud)
        #.recolor(color_func=grey_color_func, random_state=3),interpolation="bilinear")
plt.axis("off") 
plt.tight_layout(pad = 0) 
  
plt.show() 
